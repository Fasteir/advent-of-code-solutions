﻿using System.Diagnostics;
using System.Collections.Generic;

public static class ArrayExtensions
{
  public static T[] RemoveAt<T>(this T[] array, int index)
  {
    if (1 > (array?.Length ?? 0) || array.Length <= index || index < 0)
      throw new ArgumentException();
    
    for (int i = index; i < array.Length - 1; i++)
    {
      array[i] = array[i+1];
    }
    Array.Resize(ref array, array.Length -1);

    return array;
  }
}

static class Program
{
  static List<int[]> Parse(string inputPath)
  {
    List<int[]> result = new();
    
    var lines = System.IO.File.ReadAllLines(inputPath);
    foreach (var line in lines)
    {
      var reports = line.Split();
      result.Add(reports.Select(x => int.Parse(x)).ToArray());
    }

    return result;
  }

  static bool IsGradual(int a, int b)
  {
    int diff = Math.Abs(a - b);
    if (0 < diff && diff < 4)
      return true;
    return false;
  }

  static bool IsSafe(int[] levels)
  {
    var slope = levels[0].CompareTo(levels[1]);
    for (var i = 1; i < levels.Length; i++)
    {
      if (slope != levels[i-1].CompareTo(levels[i])
        || !IsGradual(levels[i-1], levels[i]))
        return false;
    }

    return true;
  }

  static long Part1(string inputPath)
  {
    var reports = Parse(inputPath);
    var safeCount = 0;

    foreach (var report in reports)
    {
      if (IsSafe(report))
        safeCount++;
    }
    
    return safeCount;
  }

  static bool IsSafeWithTolerance(int[] levels)
  {
    var slope = levels[0].CompareTo(levels[1]);
    for (var i = 1; i < levels.Length; i++)
    {
      if (slope != levels[i-1].CompareTo(levels[i])
        || !IsGradual(levels[i-1], levels[i]))
      {
        var levelsA = levels.RemoveAt(i-1);
        var levelsB = levels.RemoveAt(i);
        return IsSafe(levelsA) || IsSafe(levelsB);
      }
    }

    return true;
  }

  static long Part2(string inputPath)
  {
    var reports = Parse(inputPath);
    var safeCount = 0;

    foreach (var report in reports)
    {
      if (IsSafeWithTolerance(report))
      {
        safeCount++;
      }
    }
    
    return safeCount;
  }

  static string ToResultFormat(this TimeSpan ts, long res) => $"{res}\t{ts.TotalMilliseconds}ms";

  static long Run(this Stopwatch timer, Func<string, long> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
