﻿using System.Diagnostics;
using System.Collections.Generic;

static class Program
{
  static List<int>[] Parse(string inputPath)
  {
    List<int>[] result = [new List<int>(), new List<int>()];
    
    var lines = System.IO.File.ReadAllLines(inputPath);
    foreach (var line in lines)
    {
      var ids = line.Split();
      result[0].Add(int.Parse(ids[0]));
      result[1].Add(int.Parse(ids[3]));
    }

    return result;
  }

  static long Part1(string inputPath)
  {
    var parsed = Parse(inputPath);
    parsed[0] = parsed[0].OrderBy(x => x).ToList();
    parsed[1] = parsed[1].OrderBy(x => x).ToList();

    var sum = 0;
    for (int i = 0; i < parsed[0].Count; i++)
    {
      sum += Math.Abs(parsed[0][i] - parsed[1][i]);
    }
    
    return sum;
  }

  static long Part2(string inputPath)
  {
    var parsed = Parse(inputPath);

    var score = 0;

    foreach(var id in parsed[0])
    {
      score += id * parsed[1].Count(x => x == id);
    }

    return score;
  }

  static string ToResultFormat(this TimeSpan ts, long res) => $"{res}\t{ts.TotalMilliseconds}ms";

  static long Run(this Stopwatch timer, Func<string, long> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
