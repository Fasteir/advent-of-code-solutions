﻿using System.Diagnostics;
using System.Text.RegularExpressions;

static class Program
{
  static (int A, int B)[] Parse(string inputPath)
  {
    var text = File.ReadAllText(inputPath);
    var regex = new Regex("mul\\((\\d{1,3}),(\\d{1,3})\\)");
    var matches = regex.Matches(text);

    return matches.Select(m => (int.Parse(m.Groups[1].Value), int.Parse(m.Groups[2].Value)))
      .ToArray();
  }

  static long Part1(string inputPath)
  {
    var parsed = Parse(inputPath);
    
    var sum = 0;
    foreach (var p in parsed)
    {
      sum += p.A * p.B;
    }

    
    return sum;
  }

  static long Part2(string inputPath) => -1;

  static string ToResultFormat(this TimeSpan ts, long res) => $"{res}\t{ts.TotalMilliseconds}ms";

  static long Run(this Stopwatch timer, Func<string, long> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
