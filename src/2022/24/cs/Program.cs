﻿using System.Diagnostics;

public static class ByteExtensions
{
  // https://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetKernighan
  public static byte CountBits(this byte a)
  {
    byte count;
    for (count = 0; a > 0; count++)
      a = (byte)(a & (a - 1)); // clear the least significant bit set

    return count;
  }

  public static byte LowestBit(this byte a) => (byte)(a & -a);
}

public static class IntExtensions
{
  public static int Sign(this int a) => a >> sizeof(int) * 8 - 1;

  public static int Abs(this int a) => (a + a.Sign()) ^ a.Sign();

  public static int Mod(this int a, int b)
  {
      int r = a % b;
      return r + r.Sign().Abs() * b;
  }

  public static int Distance(this int a, int b) => (a - b).Abs();
}

static class Program
{ 
  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    // var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }

  static int Run(this Stopwatch timer, Func<string, int> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static string ToResultFormat(this TimeSpan ts, int res) => $"{res}\t{ts.ToString("s\\.ffffff")}s";

  static int Part1(string inputPath)
  {
    var valley = new Valley(inputPath);

    valley.Step(valley.MinT);

    // Console.WriteLine(valley);

    var start = new Point(-1, 0, null);

    var endPos = valley.States[0].Length - 1;
    // var end = BFS(valley, start, endPos);
    // var end = new Point(-1, 0, null);

    System.Console.WriteLine(valley);

    // var point = end;
    // var t = 0;
    // if (end.Parent != null)
    // {
    //   while(true)
    //   {
    //     point = point.Parent;
    //     t++;
    //     if (point == null)
    //       return t + 1;
    //   }
    // }
    
    return end.Time + 1;
  }

  public class Point : IEquatable<Point>
  {
    public int Position { get; init; }
    public int Time { get; init; }
    public Point? Parent { get; init; }
    public Point (int position, int time, Point? parent)
    {
      Position = position;
      Time = time;
      Parent = parent;
    }
    public bool Equals(Point? other) =>
      other is not null
      && Position == other.Position
      && Time == other.Time;
    public override bool Equals(object? obj) =>
      (obj is Point point) && Equals(point);
    public static bool operator ==(Point a, Point b) =>
        ReferenceEquals(a, b) || (a?.Equals(b) ?? false);
    public static bool operator !=(Point a, Point b) => !(a == b);
    public override int GetHashCode() => HashCode.Combine(Position, Time);
    public override string ToString() => (Position, Time).ToString();
  }

  public record struct Valley
  {
    private int _currentT = 0;
    private bool _cycleReached = false;
    private readonly int InitialStateHash;
    public readonly List<byte[]> States = new();
    public readonly List<int[]> Blizzards = new();
    public readonly int Width = 0;
    public readonly int Height = 0;
    public readonly int MinT => Width + Height + 1;
    public readonly int MaxT => States.Count;

    public Valley(string inputPath)
    {
      var initialState = new List<byte[]>();
      var initialBlizzards = new List<int>();
      var blizzardToByte = new Dictionary<char, byte>()
      {
        { '^', 1 },
        { '>', 2 },
        { 'v', 4 },
        { '<', 8 }
      };

      var y = -2;
      var lines = File.ReadLines(inputPath);
      foreach (var line in lines)
      {
        y++;
        
        if (line[1] == '#')
          break;
        
        if (y < 0)
        {
          Width = line.Length - 2;
          continue;
        }

        var innerLine = line[1..^1];
        var byteLine = new byte[innerLine.Length];
        for (int i = 0; i < innerLine.Length; i++)
        {
          var ch = innerLine[i];
          if (!blizzardToByte.Keys.Contains(ch))
            continue;

          initialBlizzards.Add(i + y * Width);
          byteLine[i] = blizzardToByte[ch];
        }
        
        initialState.Add(byteLine);
      }
      Height = y;
      
      var initialStateBytes = initialState.SelectMany(b => b);
      InitialStateHash = initialStateBytes.ToHashCode();
      
      States.Add(initialStateBytes.ToArray());
      Blizzards.Add(initialBlizzards.ToArray());
    }

    public void Step(int t = 1)
    {
      if (t < 1)
        return;

      if (_cycleReached)
      {
        _currentT = (_currentT + t) % MaxT;
        return;
      }
      
      for (int dt = t; dt > 0; dt--)
      {
        var width = Width;
        var height = Height;
        int toNewCardinalPos(int pos, byte dir)
        {
            var row = pos / width;
            var rowStart = row * width;

            return dir switch
            {
                1 => (pos - width).Mod(width * height),
                2 => (pos + 1).Mod(width) + rowStart,
                4 => (pos + width).Mod(width * height),
                8 => (pos - 1).Mod(width) + rowStart,
                _ => throw new ArgumentException("Invalid direction", nameof(dir))
            };
        }

        var newState = new byte[States[0].Length];
        var newBlizzards = new int[Blizzards[0].Length];
        var collisions = new Dictionary<int, byte>();
        for (int i = 0; i < Blizzards[_currentT].Length; i++)
        {
          int pos = Blizzards[_currentT][i];
          byte dir = States[_currentT][pos];

          var bitCount = dir.CountBits();
          if (bitCount < 1)
            throw new InvalidOperationException();

          var opDir = dir;
          if (bitCount > 1)
          {
            if (!collisions.ContainsKey(pos))
              collisions[pos] = dir;
            opDir = collisions[pos].LowestBit();
            collisions[pos] -= opDir;
          }

          var newPos = toNewCardinalPos(pos, opDir);
          newState[newPos] += opDir;
          newBlizzards[i] = newPos;
        }

        var newStateHash = newState.ToHashCode();
        if (InitialStateHash == newStateHash && States[0].SpanEquals(newState))
        {
          _cycleReached = true;
          _currentT %= MaxT;
          continue;
        }

        _currentT++;
        States.Add(newState);
        Blizzards.Add(newBlizzards);
      }
    }

    public readonly override string ToString()
    {
      var builder = new System.Text.StringBuilder();
      var byteToBlizzard = (byte b) => b switch
      {
        0 => '.',
        1 => '^',
        2 => '>',
        4 => 'v',
        8 => '<',
        _ => b.CountBits().ToString()[0]
      };

      builder.Append($"w {Width}, h {Height}, t_min {MinT}, t_max {States.Count - 1}");
      
      var width = Width;
      for (var t = 0; t < States.Count; t++)
      {
        var state = States[t];
        var blizzIds = string.Join(
          "|",
          Blizzards[t].Select(b => $"{b % width},{b / width}")
        );

        builder.Append($"{Environment.NewLine}t: {t}, b: {blizzIds}");

        var lines = state.Length / width;
        for (int lineNum = 0; lineNum < lines; lineNum++ )
        {
          var line = new byte[width];
          Array.Copy(state, lineNum * width, line, 0, width);

          builder.Append($"{Environment.NewLine}{string.Join("", line.Select(b => byteToBlizzard(b)))}");
        }

        builder.Append(Environment.NewLine);
      }

      return builder.ToString();
    }
  }

  public static int ToHashCode<T>(this IEnumerable<T> items) =>
    items.Aggregate(0, (total, next) => HashCode.Combine(total, next));

  // https://stackoverflow.com/questions/43289/comparing-two-byte-arrays-in-net/48599119#48599119
  public static bool SpanEquals(this byte[] a, byte[] b) => new ReadOnlySpan<byte>(a).SequenceEqual(b);

  public static Point BFS(Valley valley, Point start, int endPosition)
  {
    var queue = new PriorityQueue<Point, int>(Comparer<int>.Create((a,b) => a - b));
    // var queue = new Queue<Point>();
    var exploredPoints = new HashSet<Point> { start };

    var adjustedStart = start;
    while (adjustedStart.Position < 0)
    {
      if (adjustedStart.Time >= valley.MaxT)
        valley.Step();

      var space = valley.States[(adjustedStart.Time + 1) % valley.MaxT][0];
      adjustedStart = new Point(
        space == 0
          ? 0
          : adjustedStart.Position,
        adjustedStart.Time + 1,
        adjustedStart);
    }

    var endPos = valley.States[0].Length - 1;
    var getPointCost = (Point pt, int target) => pt.Time + 1 + target - pt.Position;

    queue.Enqueue(adjustedStart, getPointCost(adjustedStart, endPos));
    // queue.Enqueue(adjustedStart);
    while (queue.Count > 0)
    {
      var point = queue.Dequeue();
      if (point.Position == endPos || point.Time > 1000)
        return point;
      
      var walkablePoints = GetWalkablePointsBFS(valley, point, endPosition);
      foreach (var walkablePoint in walkablePoints)
      {
        if (false == exploredPoints.Contains(walkablePoint))
        {
          exploredPoints.Add(walkablePoint);
          queue.Enqueue(walkablePoint, getPointCost(walkablePoint, endPos));
          // queue.Enqueue(walkablePoint);
        }
      }
    }

    return new Point(-1, -1, null);
  }

  public static List<Point> GetWalkablePointsBFS(Valley valley, Point currentPoint, int endPosition)
  {
    var curPos = currentPoint.Position;
    var newTime = currentPoint.Time + 1;
    var result = new List<Point>
    {
      new(curPos + valley.Width, newTime, currentPoint),
      new(curPos + 1, newTime, currentPoint),
      new(curPos - 1, newTime, currentPoint),
      new(curPos - valley.Width, newTime, currentPoint),
      new(curPos, newTime, currentPoint),
    };

    if (newTime >= valley.MaxT)
      valley.Step();

    var width = valley.Width;
    var row = curPos / width;
    var rowStart = row * width;
    var rowEnd = rowStart + width - 1;
    var maxPos = valley.States[0].Length;

    var nextBlizzards = valley.Blizzards[newTime % valley.MaxT];

    result = result
      .Where(point => point.Position > -1 && point.Position < maxPos)
      .Where(point => point.Position.Distance(curPos) != 1
                   || (  point.Position.CompareTo(curPos) < 0
                      && point.Position >= rowStart)
                   || (  point.Position.CompareTo(curPos) > 0
                      && point.Position <= rowEnd)
      )
      .Where(point => false == nextBlizzards.Contains(point.Position))
      .ToList();

    return result;
  }

  static int Part2(string inputPath) => -1;
}
