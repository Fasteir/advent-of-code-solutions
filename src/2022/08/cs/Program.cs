﻿static class Program
{
  static int ToNorth(this int pos, int width, int length = 0) => pos - width < 0 ? -1 : pos - width;
  static int ToEast(this int pos, int width, int length = 0) => (pos + 1) % width == 0 ? -1 : pos + 1;
  static int ToSouth(this int pos, int width, int length = 0) => pos + width >= length ? -1 : pos + width;
  static int ToWest(this int pos, int width, int length = 0) => (pos - 1) % width == width - 1 ? -1 : pos - 1;
  static int ToEdgeNorth(this int pos, int width) => pos % width;
  static int ToEdgeEast(this int pos, int width) => pos + width - (pos % width) - 1;
  static int ToEdgeSouth(this int pos, int width, int length = 0) => length - width + pos.ToEdgeNorth(width);
  static int ToEdgeWest(this int pos, int width) => (pos / width) * width;
  static bool IsAtEdge(this int pos, int width, int length) => ToNorth(pos, width) == -1
                                                            || ToEast(pos, width) == -1
                                                            || ToSouth(pos, width, length) == -1
                                                            || ToWest(pos, width) == -1;
  static char Max(char a, char b) => b.CompareTo(a) > 0 ? b : a;
  static bool IsVisibleFromDirection(this int pos,
                     Func<int, int, int, int> toDirection,
                                       string map,
                                          int width,
                                          int length,
                                          int edgePos)
  {
    var cursor = edgePos;
    var highest = '0';
    do
    {
      highest = Max(highest, map[cursor]);
      if (map[pos] <= highest)
        return false;
    } while ((cursor = toDirection(cursor, width, length)) != pos);
    return true;
  }
  static bool IsVisibleFromOutside(this int pos, string map, int width)
    => pos.IsAtEdge(width, map.Length)
    || pos.IsVisibleFromDirection(ToSouth, map, width, map.Length, pos.ToEdgeNorth(width))
    || pos.IsVisibleFromDirection(ToWest, map, width, map.Length, pos.ToEdgeEast(width))
    || pos.IsVisibleFromDirection(ToNorth, map, width, map.Length, pos.ToEdgeSouth(width, map.Length))
    || pos.IsVisibleFromDirection(ToEast, map, width, map.Length, pos.ToEdgeWest(width));
  static int CountPositionsVisibleFromOutside(this string map, int width)
  {
    var count = 0;
    for (var pos = 0; pos < map.Length; pos++)
      if (pos.IsVisibleFromOutside(map, width))
        count++;
    return count;
  }
  static int Part1(string filePath)
  {
    var lines = System.IO.File.ReadAllLines(filePath);
    var map = string.Join("", lines);
    var width = lines[0].Length;
    var numVis = map.CountPositionsVisibleFromOutside(width);
    return numVis;
  }
  static int Product(this IEnumerable<int> collection) => collection.Aggregate(1, (a, x) => a * x);
  static int ToDirection(this int pos, int direction, int width, int length) => direction switch
  {
    0 => pos.ToNorth(width),
    1 => pos.ToEast(width),
    2 => pos.ToSouth(width, length),
    3 => pos.ToWest(width),
    _ => -1
  };
  static int ToVisibilityScore(this int pos, string map, int width)
  {
    if (pos.IsAtEdge(width, map.Length))
      return 0;
    
    var scores = new int[4];
    for (var direction = 0; direction < scores.Length; direction++)
    {
      var cursor = pos;
      do
      {
        cursor = cursor.ToDirection(direction, width, map.Length);
        scores[direction]++;
      } while (cursor >= 0 && map[pos] > map[cursor] && cursor.IsAtEdge(width, map.Length) == false);
    }
    return scores.Product();
  }
  static int ToVisibilityScoreMax(this string map, int width)
  {
    var scoreMax = 0;
    for (var pos = width + 1; pos < map.Length; pos++)
      scoreMax = System.Math.Max(scoreMax, pos.ToVisibilityScore(map, width));
    return scoreMax;
  }
  static int Part2(string filePath)
  {
    var lines = System.IO.File.ReadAllLines(filePath);
    var map = string.Join("", lines);
    var width = lines[0].Length;
    var scoreMax = map.ToVisibilityScoreMax(width);
    return scoreMax;
  }
  static string ToResultFormat(this TimeSpan ts, int result) => $"{result}\t{ts.ToString("s\\.ffffff")}s";
  static int Run(this System.Diagnostics.Stopwatch timer, Func<string, int> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }
  static void Main(string[] args)
  {
    var timer = new System.Diagnostics.Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}