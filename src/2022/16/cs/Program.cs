﻿public static class LongExtensions
{
  public static long Get(this long a, int b) => (a >> b) & 1L;
  public static long Set(this long a, int b) => a | (1L << b);
}

static class Program
{
  record ValveGraph(int[,] distances, Valve[] Valves);
  record Valve(int Id, string Name, int FlowRate, string[] Adjacent);

  static Valve[] ParseValves(string inputPath)
  {
    var lines = System.IO.File.ReadLines(inputPath);
    var valves = new HashSet<Valve>();
    var valveId = 0;
    foreach (var line in lines)
    {
      if (string.IsNullOrWhiteSpace(line))
        continue;

      var pattern = "^Valve ([A-Z]+)\\D*(\\d+)[^A-Z]*([^$]+)";
      var match = System.Text.RegularExpressions.Regex.Match(line, pattern);
      var name = match.Groups[1].Value;
      var flowRate = int.Parse(match.Groups[2].Value);
      var adjacent = match.Groups[3].Value.Split(", ");
      
      var valve = new Valve(valveId++, name, flowRate, adjacent);
      valves.Add(valve);
    }

    return valves.ToArray();
  }

  static int[,] GetDistances(Valve[] valves)
  {
    var n = valves.Length;
    var distances = new int[n, n];
    if (n == 0)
      return distances;

    // Initialize adjacency matrix
    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        if (i == j)
          distances[i,j] = 0;
        else if (valves[i].Adjacent.Contains(valves[j].Name))
          distances[i,j] = 1;
        else
          distances[i,j] = int.MaxValue;

    // Apply Floyd Warshall O(V^3) for cost matrix
    for (int mid = 0; mid < n; mid++)
      for (int start = 0; start < n; start++)
        for (int end = 0; end < n; end++)
        {
          var dist = distances[start, mid] + distances[mid, end];
          if ( distances[start, mid] != int.MaxValue
            && distances[mid, end] != int.MaxValue
            && dist < distances[start, end])
            distances[start, end] = dist;

          if (distances[start, start] < 0)
            return distances;
        }
    
    return distances;
  }

  static int GetFlowMax(Dictionary<string, int> cache, ValveGraph graph, int duration, Valve current, long visitedMask)
  {
    var state = $"{duration}:{current.Id}:{visitedMask}";
    if (false == cache.ContainsKey(state))
    {
      var valveFlow = current.FlowRate * duration;

      var flowLeft = 0;
      var valvesLeft = graph.Valves.Where(v => visitedMask.Get(v.Id) != 1);
      foreach (var valve in valvesLeft)
      {
        var distance = graph.distances[current.Id, valve.Id];
        if (duration > distance)
          flowLeft = Math.Max(
            flowLeft,
            GetFlowMax(cache, graph, duration - distance - 1, valve, visitedMask.Set(valve.Id))
          );
      }
      cache[state] = valveFlow + flowLeft;
    }

    return cache[state];
  }

  static int Part1(string inputPath)
  {
    var valves = ParseValves(inputPath);
    var start = valves.First(x => x.Name.Equals("AA"));
    var visitedMask = valves.Where(valve => valve.FlowRate <= 0).Sum(v => 0L.Set(v.Id));
    var graph = new ValveGraph(GetDistances(valves), valves);
    
    var cache = new Dictionary<string, int>();
    var result = GetFlowMax(cache, graph, 30, start, visitedMask);

    return result;
  }

  static int Part2(string inputPath)
  {
    return -1;
  }

  static string ToResultFormat(this System.TimeSpan ts, int res) => $"{res}\t{ts.ToString("s\\.ffffff")}s";
  static int Run(this System.Diagnostics.Stopwatch timer, Func<string, int> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }
  static void Main(string[] args)
  {
    var timer = new System.Diagnostics.Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
