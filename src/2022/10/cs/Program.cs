﻿static class Program
{
  enum Op
  {
    NoOp,
    AddX
  }
  static Dictionary<Op, int> OpToCycles = new Dictionary<Op, int>()
  {
    { Op.NoOp, 1 },
    { Op.AddX, 2 }
  };
  public static int Sign(this int a) => a >> sizeof(int) * 8 - 1;
  public static int Abs(this int a) => (a + a.Sign()) ^ a.Sign();
  public static int Mod(this int a, int b)
  {
      int r = a % b;
      return r + r.Sign().Abs() * b;
  }
  public static int ToDistanceFrom(this int a, int b) => (a - b).Abs();
  public static int ToHalf(this int a) => a >> 1;

  static bool TryParseInstruction(string line, out (Op op, int arg) result)
  {
    var instruction = line.Split(' ');
    result = (op: Op.NoOp, arg: 0);
    
    if (false == Enum.TryParse<Op>(instruction[0], ignoreCase: true, out Op op))
      return false;
    
    int arg = 0;
    if (instruction.Length > 1 && false == int.TryParse(instruction[1], out arg))
    {
      result.arg = arg;
      return false;
    }

    result.op = op;
    result.arg = arg;
    
    return true;
  }

  static int Solve(string inputPath, bool drawToScreen = false)
  {
    var lines = System.IO.File.ReadLines(inputPath);
    var lineScanner = lines.GetEnumerator();
    var opQueue = new System.Collections.Generic.LinkedList<(Op op, int arg)>();

    var completedTicks = 0;
    var registerX = 1;
    var signalStrengthSum = 0;
    
    var screenWidth = 40;
    var screenBuffer = new System.Text.StringBuilder();
    var spriteWidth = 3;

    var completeFirstOp = ((Op op, int arg) instruction) =>
    {
      opQueue.RemoveFirst();
      if (instruction.op == Op.AddX)
        return instruction.arg;
      return 0;
    };
    var parseAndQueueOp = (string line) =>
    {
      if (false == TryParseInstruction(line, out (Op op, int arg) instruction))
        return false;

      var cycles = OpToCycles[instruction.op];
      for (int cycle = 1; cycle < cycles; cycle++)
        opQueue.AddLast((Op.NoOp, 0));
      opQueue.AddLast(instruction);
      return true;
    };
    var posToPixel = (int pos, int spriteCenter, int spriteWidth) =>
    {
      if (pos.ToDistanceFrom(registerX) < (spriteWidth + 1).ToHalf())
        return '#';
      return '.';
    };

    do
    {
      if (opQueue.First != null)
        registerX += completeFirstOp(opQueue.First.Value);
      
      if (lineScanner.MoveNext())
        _ = parseAndQueueOp(lineScanner.Current);

      if (drawToScreen)
      {
        var linePos = completedTicks.Mod(screenWidth);
        screenBuffer.Append(posToPixel(linePos, registerX, spriteWidth));
        if (linePos >= screenWidth - 1)
          screenBuffer.AppendLine();
      }
      
      completedTicks++;

      if ((completedTicks - screenWidth.ToHalf()).Mod(screenWidth) == 0)
        signalStrengthSum += completedTicks * registerX;

    } while (opQueue.Count > 0);

    if (drawToScreen)
      System.Console.WriteLine(screenBuffer.ToString());

    return signalStrengthSum;
  }

  static string ToResultFormat(this TimeSpan ts, int result) => $"{result}\t{ts.ToString("s\\.ffffff")}s";
  static int Run(this System.Diagnostics.Stopwatch timer, Func<string, bool, int> solution, string inputPath, bool display = false)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath, display);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new System.Diagnostics.Stopwatch();
    var warmup = timer.Run(Solve, args[0]);
    var part1 = timer.Run(Solve, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Solve, args[0], true);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}