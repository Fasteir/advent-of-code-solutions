﻿using System.Diagnostics;

public static class IntExtensions
{
  public static int Sign(this int a) => a >> sizeof(int) * 8 - 1;
  public static int Abs(this int a) => (a + a.Sign()) ^ a.Sign();
  public static int Distance(this int a, int b) => (a - b).Abs();
}
public class Tile : IEquatable<Tile>
{
  public int X { get; set; }
  public int Y { get; set; }
  public char Z { get; set; }
  public int Cost { get; set; }
  public int Distance { get; set; }
  public int CostDistance => Cost + Distance;
  public Tile? Parent { get; set; }
  public int GetDistance(int targetX, int targetY) => Distance = X.Distance(targetX) + Y.Distance(targetY); 
  public bool Equals(Tile? other) => other != null && X == other.X && Y == other.Y && Z == other.Z;
  public override string ToString() => (X, Y, Z).ToString();
}
static class Program
{
  private const char Start = 'S';
  private const char End = 'E';
  private static List<Tile> GetWalkableTilesAStar(List<string> map, Tile current, Tile target)
  {
    var possibleTiles = new List<Tile>()
    {
      new Tile { X = current.X, Y = current.Y - 1, Z = ' ', Parent = current, Cost = current.Cost + 1 },
      new Tile { X = current.X, Y = current.Y + 1, Z = ' ', Parent = current, Cost = current.Cost + 1 },
      new Tile { X = current.X - 1, Y = current.Y, Z = ' ', Parent = current, Cost = current.Cost + 1 },
      new Tile { X = current.X + 1, Y = current.Y, Z = ' ', Parent = current, Cost = current.Cost + 1 }
    };

    var maxX = map.First().Length;
    var maxY = map.Count;

    possibleTiles = possibleTiles
      .Where(tile => tile.X >= 0 && tile.X < maxX)
      .Where(tile => tile.Y >= 0 && tile.Y < maxY)
      .ToList();

    possibleTiles.ForEach(tile => tile.Z = map[tile.Y][tile.X]);

    possibleTiles = possibleTiles
      .Where(tile => (map[tile.Y][tile.X] - current.Z) < 2 && map[tile.Y][tile.X] != End
                  || map[tile.Y][tile.X] == End
                  || current.Z == Start)
      .ToList();
    
    possibleTiles.ForEach(tile => tile.Distance = tile.GetDistance(target.X, target.Y));

    return possibleTiles;
  }
  static Tile AStar(List<string> map, Tile startTile, Tile endTile)
  {
    var queuedTiles = new List<Tile>();
    queuedTiles.Add(startTile);
    var exploredTiles = new List<Tile>();

    while(queuedTiles.Any())
    {
      var selectedTile = queuedTiles.OrderBy(tile => tile.CostDistance).First();
      if(selectedTile.Equals(endTile))
        return selectedTile;

      exploredTiles.Add(selectedTile);
      queuedTiles.Remove(selectedTile);

      var walkableTiles = GetWalkableTilesAStar(map, selectedTile, endTile);
      foreach(var walkableTile in walkableTiles)
      {
        if (exploredTiles.Contains(walkableTile))
          continue;

        var existingTile = queuedTiles.FirstOrDefault(tile => tile.Equals(walkableTile));
        if (existingTile == default)
        {
          queuedTiles.Add(walkableTile);
          continue;
        }
        
        if(existingTile.CostDistance > selectedTile.CostDistance)
        {
          queuedTiles.Remove(existingTile);
          queuedTiles.Add(walkableTile);
        }
      }
    }

    return new Tile()
    {
      X = -1,
      Y = -1,
      Z = '\0'
    };
  }
  static int Part1(string inputPath, char newStartCh = Start)
  {
    var lines = System.IO.File.ReadLines(inputPath);
    
    List<string> map = new List<string>();
    foreach (var line in lines)
      map.Add(line);

    var end = new Tile();
    end.Y = map.FindIndex(x => x.Contains(End));
    end.X = map[end.Y].IndexOf(End);
    end.Z = End;

    var start = new Tile();
    start.Y = map.FindIndex(x => x.Contains(Start));
    start.X = map[start.Y].IndexOf(Start);
    start.Z = Start;
    start.Distance = start.GetDistance(end.X, end.Y);

    var tile = AStar(map, start, end);

    if (tile.Z == '\0')
      return -1;

    var steps = 0;
    var pathTile = tile;
    while(true)
    {
      if(pathTile.Parent == null)
        break;
      steps++;
      pathTile = pathTile.Parent;
    }

    return steps;
  }

  static List<Tile> GetWalkableTilesBFS(List<string> map, Tile currentTile, char endCh)
  {
    var walkableTiles = new List<Tile>()
    {
      new Tile { X = currentTile.X, Y = currentTile.Y - 1, Parent = currentTile },
      new Tile { X = currentTile.X, Y = currentTile.Y + 1, Parent = currentTile },
      new Tile { X = currentTile.X - 1, Y = currentTile.Y, Parent = currentTile },
      new Tile { X = currentTile.X + 1, Y = currentTile.Y, Parent = currentTile },
    };

    var maxX = map.First().Length;
    var maxY = map.Count();

    walkableTiles = walkableTiles
      .Where(tile => tile.X >= 0 && tile.X < maxX)
      .Where(tile => tile.Y >= 0 && tile.Y < maxY)
      .ToList();
    
    walkableTiles.ForEach(tile => tile.Z = map[tile.Y][tile.X]);

    walkableTiles = walkableTiles.Where(tile => (currentTile.Z - map[tile.Y][tile.X]) < 2
                              || tile.Z == End && currentTile.Z == 'z'
                              || tile.Z == 'z' && currentTile.Z == End
                              || tile.Z == Start && currentTile.Z == 'a'
                              || tile.Z == 'a' && currentTile.Z == Start)
                              .ToList();

    return walkableTiles;
  }

  static Tile BFS(List<string> map, Tile startTile, char endCh)
  {
    var queue = new Queue<Tile>();
    var exploredTiles = new List<Tile>();
    
    exploredTiles.Add(startTile);
    queue.Enqueue(startTile);
    while (queue.Any())
    {
      var tile = queue.Dequeue();
      if (tile.Z == endCh)
        return tile;
      var walkableTiles = GetWalkableTilesBFS(map, tile, endCh);
      foreach (var walkableTile in walkableTiles)
      {
        if (false == exploredTiles.Contains(walkableTile))
        {
          exploredTiles.Add(walkableTile);
          walkableTile.Parent = tile;
          queue.Enqueue(walkableTile);
        }
      }
    }

    return new Tile()
    {
      X = -1,
      Y = -1,
      Z = '\0'
    };
  }
  static int Part2(string inputPath, char newStartCh)
  {
    var startCh = End;
    var lines = System.IO.File.ReadLines(inputPath);
    
    List<string> map = new List<string>();
    foreach (var line in lines)
      map.Add(line);

    var start = new Tile();
    start.Y = map.FindIndex(x => x.Contains(startCh));
    start.X = map[start.Y].IndexOf(startCh);
    start.Z = startCh;

    var shortestSteps = int.MaxValue;
    var endTile = BFS(map, start, newStartCh);

    var tile = endTile;
    var steps = 0;
    if (endTile.Z != '\0')
    {
      while(true)
        {
          tile = tile.Parent;
          steps++;
          if(tile == null)
            return steps - 1;
        }
    }

    return shortestSteps;
  }
  static string ToResultFormat(this TimeSpan ts, int result) => $"{result}\t{ts.ToString("s\\.ffffff")}s";
  static int Run(this Stopwatch timer, Func<string, char, int> solution, string inputPath, char newStart = '\0')
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath, newStart);
    timer.Stop();
    return result;
  }
  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);

    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    
    var part2 = timer.Run(Part2, args[0], 'a');
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}