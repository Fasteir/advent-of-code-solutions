﻿using System.Diagnostics;

static class Program
{ 
  static long Part1(string inputPath)
  {
    var lines = System.IO.File.ReadLines(inputPath);
    var monkeys = new Dictionary<string, string>();
    foreach (var line in lines)
    {
      var monkeyOp = line.Split(':');
      monkeys[monkeyOp[0]] = monkeyOp[1].Trim();
    }

    var result = new Dictionary<string, long>();
    var pendingMonkeyStack = new Stack<string>();

    var resolveOp = (long a, long b, string op) => op switch
    {
      "+" => a + b,
      "-" => a - b,
      "*" => a * b,
      "/" => a / b,
      _ => -1
    };

    pendingMonkeyStack.Push("root");
    while (pendingMonkeyStack.Count > 0)
    {
      var monkey = pendingMonkeyStack.Pop();
      if (result.ContainsKey(monkey))
        continue;

      var monkeyOp = monkeys[monkey];
      var monkeyOpSplit = monkeyOp.Split(' ');
      if (monkeyOpSplit.Length < 2)
      {
        result[monkey] = long.Parse(monkeyOpSplit[0]);
        continue;
      }

      pendingMonkeyStack.Push(monkey);

      var monkeyA = monkeyOpSplit[0];
      var operation = monkeyOpSplit[1];
      var monkeyB = monkeyOpSplit[2];
      var anyUnknownMonkeyNumber = false;
      if (false == result.ContainsKey(monkeyA))
      {
        anyUnknownMonkeyNumber = true;
        pendingMonkeyStack.Push(monkeyA);
      }
      if (false == result.ContainsKey(monkeyB))
      {
        anyUnknownMonkeyNumber = true;
        pendingMonkeyStack.Push(monkeyB);
      }
      if (anyUnknownMonkeyNumber)
        continue;
      
      result[monkey] = resolveOp(result[monkeyA], result[monkeyB], operation);
    }

    return result["root"];
  }

  static long Part2(string inputPath) => -1L;

  static string ToResultFormat(this TimeSpan ts, long res) => $"{res}\t{ts.ToString("s\\.ffffff")}s";

  static long Run(this Stopwatch timer, Func<string, long> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
