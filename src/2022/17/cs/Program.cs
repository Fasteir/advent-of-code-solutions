﻿using System.Diagnostics;

static class Program
{
  public class Chamber
  {
    public int Width { get; private set; }
    public List<bool[]> Rocks { get; private set; }
    public int Height { get; private set; }
    public Chamber(int width)
    {
      Width = width;
      Rocks = new List<bool[]>();
      Height = 0;
    }
    public void Merge(IEnumerable<(int X, int Y)> rockPieces)
    {
      var yMin = rockPieces.Min(xy => xy.Y);
      var yMax = rockPieces.Max(xy => xy.Y);
      
      while (Rocks.Count <= yMax)
        Rocks.Add(new bool[Width]);

      foreach (var xy in rockPieces)
        Rocks[xy.Y][xy.X] = true;

      Height = Math.Max(Height, yMax + 1);
    }
  }

  public record Rock
  {
    public enum RockShape
    {
      Bar = 0,
      Cross = 1,
      BackL = 2,
      I = 3,
      Square = 4
    }

    // Each point is an offset of the rock's bottom-left corner position
    public static Dictionary<RockShape, HashSet<(int X, int Y)>> RockShapePieces =
      new Dictionary<RockShape, HashSet<(int X, int Y)>>
    {
      { RockShape.Bar,    new HashSet<(int, int)>() { (0, 0), (1, 0), (2, 0), (3, 0) } },
      { RockShape.Cross,  new HashSet<(int, int)>() { (1, 0), (0, 1), (1, 1), (2, 1), (1, 2)} },
      { RockShape.BackL,  new HashSet<(int, int)>() { (0, 0), (1, 0), (2, 0), (2, 1), (2, 2)} },
      { RockShape.I,      new HashSet<(int, int)>() { (0, 0), (0, 1), (0, 2), (0, 3) } },
      { RockShape.Square, new HashSet<(int, int)>() { (0, 0), (1, 0), (0, 1), (1, 1) } }
    };

    public RockShape Shape { get; private set; }
    public HashSet<(int X, int Y)> Pieces { get; private set; }
    public int X { get; private set; }
    public int Y { get; private set;}
    public int Height { get; private set; }
    public int Width { get; private set; }
    public Rock(RockShape shape, int x, int y)
    {
      X = x;
      Y = y;
      Shape = shape;
      Pieces = RockShapePieces[Shape];
      Height = Pieces.Max(xy => xy.Y) - Pieces.Min(xy => xy.Y) + 1;
      Width = Pieces.Max(xy => xy.X) - Pieces.Min(xy => xy.X) + 1;
    }
    public void Cycle(int x, int y)
    {
      X = x;
      Y = y;
      Shape = (RockShape)(((int)Shape + 1) % Enum.GetNames<RockShape>().Length);
      Pieces = RockShapePieces[Shape];
      Height = Pieces.Max(xy => xy.Y) - Pieces.Min(xy => xy.Y) + 1;
      Width = Pieces.Max(xy => xy.X) - Pieces.Min(xy => xy.X) + 1;
    }
    static bool AreColliding(IEnumerable<(int X, int Y)> rock, ref Chamber chamber)
    {
      foreach (var piece in rock)
      {
        if (piece.Y >= chamber.Height)
          return false;

        if (chamber.Rocks[piece.Y][piece.X])
          return true;
      }

      return false;
    }
    public bool MoveSide(ref Chamber chamber, bool isLeft)
    {
      var xOffset = isLeft ? -1 : 1;

      if (X + xOffset < 0 || X + xOffset + Width > chamber.Width)
        return false;
      
      var newRockPosition = this.Pieces.Select(xy => (xy.X + X + xOffset, xy.Y + Y));
      if (AreColliding(newRockPosition, ref chamber))
        return false;
      
      X += xOffset;
      return true;
    }

    public bool MoveDown(ref Chamber chamber)
    {
      var yOffset = -1;

      if (Y + yOffset < 0)
        return false;

      var newRockPosition = this.Pieces.Select(xy => (xy.X + X, xy.Y + Y + yOffset));
      if (AreColliding(newRockPosition, ref chamber))
        return false;

      Y += yOffset;
      return true;
    }
  }

  static LinkedList<bool> ParseJetPattern(string inputPath)
  {
    var moves = System.IO.File.ReadAllText(inputPath).Where(m => "<>".Contains(m));
    var jetPattern = new LinkedList<bool>();
    foreach (var move in moves)
      jetPattern.AddLast(move == '<' ? true : false);

    return jetPattern;
  }

  public static void DrawChamber(Chamber chamber, Rock? rock)
  {
    var width = chamber.Width;
    var height = Math.Max(chamber.Height, rock?.Height + rock?.Y ?? 0);

    var displayLine = Enumerable.Repeat('.', width).Prepend('|').Append('|');
    var displayMap = new char[height + 1][];
    
    var yLength = displayMap.GetLength(0);
    displayMap[0] = Enumerable.Repeat('-', width).Prepend('+').Append('+').ToArray();
    for (int y = yLength - 1; y > 0; y--)
      displayMap[y] = displayLine.ToArray();
    
    if (rock != null)
      foreach (var rockPiece in rock.Pieces)
        displayMap[rockPiece.Y + rock.Y + 1][rockPiece.X + rock.X + 1] = '@';
    
    var rocks = chamber.Rocks
      .Select((y, row) => y.Contains(true) ? row : -1)
      .Where(row => row != -1)
      .SelectMany(row => chamber.Rocks[row].Select((x, col) => (Y: row, X: x ? col : -1)))
      .Where(xy => xy.X != -1);
    foreach (var chamberRock in rocks)
      displayMap[chamberRock.Y + 1][chamberRock.X + 1] = '#';

    for (int y = yLength - 1; y >= 0; y--)
      Console.WriteLine(displayMap[y]);
  }

  static int Solve(string inputPath, long maxRocks)
  {
    var sideMovePattern = ParseJetPattern(inputPath);
    if (sideMovePattern == null || sideMovePattern.Count < 1 || sideMovePattern.First == null)
      return -1;

    LinkedListNode<bool>? sideMove = null;

    var chamberWidth = 7;
    var chamber = new Chamber(chamberWidth);
    
    var newRockNeeded = false;
    var newRockOffsetX = 2;
    var newRockOffsetY = 3;
    var currentRockShape = Rock.RockShape.Bar;
    Rock currentRock = new Rock(currentRockShape, newRockOffsetX, chamber.Height + newRockOffsetY);

    var rocksDropped = 0;
    while(true)
    {
      // Create new block at 2, 0 (3 from highest rock) if none currently falling
      if (newRockNeeded)
      {
        currentRock.Cycle(newRockOffsetX, chamber.Height + newRockOffsetY);
        newRockNeeded = false;
      }

      // Make side move
      if (sideMove == null)
        sideMove = sideMovePattern.First;
      _ = currentRock.MoveSide(ref chamber, sideMove.Value);
      sideMove = sideMove.Next;

      // Make downward move, if possible
      var success = currentRock.MoveDown(ref chamber);
      if (false == success)
      {
        // else settle and flag for new rock
        var settledRocks = currentRock.Pieces.Select(xy => (X: currentRock.X + xy.X, Y: currentRock.Y + xy.Y));
        chamber.Merge(settledRocks);
        
        newRockNeeded = true;
        rocksDropped++;
        if (rocksDropped >= maxRocks)
          break;
      }
    }

    // DrawChamber(chamber, currentRock);

    return chamber.Height;
  }

  static int Part1(string inputPath) => Solve(inputPath, 2022);
  
  static int Part2(string inputPath) => true ? -1 : Solve(inputPath, 1_000_000_000_000);

  static string ToResultFormat(this TimeSpan ts, int res) => $"{res}\t{ts.ToString("s\\.ffffff")}s";
  static int Run(this Stopwatch timer, Func<string, int> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }
  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
