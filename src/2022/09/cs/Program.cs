﻿static class Program
{
  public static ((int, int), int) ParseMotion(string[] motion) =>
  (
    motion[0] switch
    {
      "L" => (-1, 0),
      "R" => (1, 0),
      "D" => (0, -1),
      "U" => (0, 1),
      _ => (0, 0)
    },
    int.TryParse(motion[1], out int distance) ? distance : 0
  );
  public static int Sign(this int a) => a >> sizeof(int) * 8 - 1;
  public static int Abs(this int a) => (a + a.Sign()) ^ a.Sign();
  public static int ToDistanceFrom(this int a, int b) => (a - b).Abs();
  public static (int, int) ShiftPer(this (int x, int y) tail, (int x, int y) head)
  {
    (int x, int y) res = tail;
    var xDiff = head.x - tail.x;
    var yDiff = head.y - tail.y;
    var xDist = xDiff.Abs();
    var yDist = yDiff.Abs();
    
    if (xDist == 1 && yDist > 1 || xDist > 1)
      res.x += xDiff / xDist;

    if (xDist > 1  && yDist > 0 || xDist < 2  && yDist > 1)
      res.y += yDiff / yDist;

    return res;
  }
  public static bool IsTouching(this (int x, int y) a, (int x, int y) b) =>
    a.x.ToDistanceFrom(b.x) < 2 && a.y.ToDistanceFrom(b.y) < 2;
  static int Solve(string inputPath, int knots)
  {
    if (knots < 2)
      return -1;
    
    var lines = System.IO.File.ReadLines(inputPath);
    var rope = new (int x, int y)[knots];
    var tailVisits = new Dictionary<(int, int), bool>() { {(0, 0), true} };
    foreach(var line in lines)
    {
      ((int x, int y) motion, int distance) = ParseMotion(line.Split(' '));
      for (int step = 0; step < distance; step++)
      {
        rope[0] = (rope[0].x + motion.x, rope[0].y + motion.y);
        for (int current = 1; current < rope.Length; current++)
        {
          if (rope[current - 1].IsTouching(rope[current]))
            continue;
          
          rope[current] = rope[current].ShiftPer(rope[current - 1]);
          if (current - 1 > knots - 3)
            tailVisits[rope[current]] = true;
        }
      }
    }
    return tailVisits.Keys.Count;
  }
  static string ToResultFormat(this TimeSpan ts, int result) => $"{result}\t{ts.ToString("s\\.ffffff")}s";
  static int Run(this System.Diagnostics.Stopwatch timer, Func<string, int, int> solution, string inputPath, int knots)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath, knots);
    timer.Stop();
    return result;
  }
  static void Main(string[] args)
  {
    var timer = new System.Diagnostics.Stopwatch();
    var warmup = timer.Run(Solve, args[0], 2);
    var part1 = timer.Run(Solve, args[0], 2);
    Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Solve, args[0], 10);
    Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}