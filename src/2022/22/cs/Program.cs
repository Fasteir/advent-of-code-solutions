﻿using System.Diagnostics;

public static class IntExtensions
{
  private static int BitsInByte = 8;
  public static int Sign(this int a) => a >> sizeof(int) * BitsInByte - 1;
  public static int Abs(this int a) => (a + a.Sign()) ^ a.Sign();
  public static int Mod(this int a, int b)
  {
    var remainder = a % b;
    return remainder + remainder.Sign().Abs() * b;
  }
  public static int IsOdd(this int a) => a & 1;
  public static int Not(this int a) => a ^ 1;
  public static int Negate(this int a) => (a ^ -1) + 1;
  public static int Eq(this int a, int b) => 1 + ((a-b) | (b-a)).Sign();
  public static int Gt(this int a, int b) => (b - a).Sign().Abs();
}

static class Program
{ 
  enum Direction
  {
    Right = 0,
    Down = 1,
    Left = 2,
    Up = 3
  }

  static Dictionary<string, int> Turn = new Dictionary<string, int>()
  {
    { "R",  1 },
    { "L", -1 }
  };
  
  static char Floor = '.';
  static char Void = ' ';
  static char Wall = '#';

  static Dictionary<Direction, char> Facing = new Dictionary<Direction, char>
  {
    { Direction.Right,  '>'},
    { Direction.Down,   'v' },
    { Direction.Left,   '<' },
    { Direction.Up,     '^' }
  };

  record BoardRow
  {
    public readonly int Offset;
    public readonly int FloorLength;
    public readonly int TotalLength;
    public readonly HashSet<int> WallIndexes;

    public BoardRow(int offset, int length, HashSet<int> wallIndexes)
    {
      Offset = offset;
      FloorLength = length;
      TotalLength = Offset + FloorLength;
      WallIndexes = wallIndexes;
    }

    public override string ToString() =>
      string.Join("", Enumerable.Range(0, TotalLength)
        .Select(i => i < Offset ? Void : (WallIndexes.Contains(i) ? Wall : Floor)));
  }

  record Notes
  {
    public readonly List<BoardRow> Board = new List<BoardRow>();
    public int Height => Board.Count;
    public readonly int Width = 0;
    public readonly int[] Path;
    public readonly (int X, int Y) Start = (-1, -1);

    public Notes(IEnumerable<string> lines)
    {
      var parsingBoard = true;
      var initializeStart = true;
      var path = new List<int>();

      foreach (var line in lines)
      {
        if (line.Length == 0)
        {
          parsingBoard = false;
          continue;
        }

        if (parsingBoard)
        {
          Width = System.Math.Max(Width, line.Length);

          var offset = 0;
          while (offset < line.Length && char.IsWhiteSpace(line[offset]))
            offset++;

          var wallIndexes = new HashSet<int>();
          for (var x = offset; x < line.Length; x++)
          {
            var isWall = line[x] == Wall;
            if (isWall)
            {
              wallIndexes.Add(x);
              continue;
            }

            if (initializeStart)
            {
              Start = (X: x, Y: Height);
              initializeStart = false;
            }
          }

          Board.Add(new BoardRow(offset, line.Length - offset, wallIndexes));
          continue;
        }

        // Parse path
        var buffer = new System.Text.StringBuilder();
        foreach (var ch in line)
        {
          if (char.IsLetter(ch))
          {
            path.Add(int.Parse(buffer.ToString()));
            path.Add(Turn[ch.ToString()]);
            buffer.Clear();
            continue;
          }

          buffer.Append(ch);
        }

        path.Add(int.Parse(buffer.ToString()));
        break;
      }

      Path = path.ToArray();
    }

    public void Print(Dictionary<(int X, int Y), char>? visited)
    {
      System.Console.WriteLine($"{Width}, {Height}, @{Start}");

      if (visited == null)
        foreach(var row in Board)
          System.Console.WriteLine(row);
      else
        for (var row = 0; row < Board.Count; row++)
        {
          var r = Board[row].ToString().ToCharArray();
          var pts = visited.Keys.Where(pt => pt.Y == row);
          foreach (var pt in pts)
            r[pt.X] = visited[pt];
          
          System.Console.WriteLine(r);
        }
        
      System.Console.WriteLine(string.Join(",", Path));
    }
  }

  static int Part1(string inputPath)
  {
    var notes = new Notes(System.IO.File.ReadLines(inputPath));
    // notes.Print(null);

    var pointX = notes.Start.X;
    var pointY = notes.Start.Y;
    var face = Direction.Right;
    var directionCount = Facing.Values.Count;
    var isTurning = false;
    var visited = new Dictionary<(int X, int Y), char>{ { notes.Start, Facing[face] } };

    foreach (var instruction in notes.Path)
    {
      if (isTurning)
      {
        face = (Direction)((int)face + instruction).Mod(directionCount);
        visited[(pointX, pointY)] = Facing[face];
        isTurning = false;
        continue;
      }

      for (var steps = instruction; steps > 0; steps--)
      {
        var intFace = (int)face;
        var isFacingHorizontal = intFace.IsOdd() == 0;
        var stepFacing = intFace > 1 ? -1 : 1;
        
        if (isFacingHorizontal)
        {
          // X
          var row = notes.Board[pointY];
          var newpointX = (pointX + stepFacing - row.Offset)
            .Mod(row.FloorLength)
            + row.Offset;

          if (row.WallIndexes.Contains(newpointX))
            break;

          pointX = newpointX;
          visited[(pointX, pointY)] = Facing[face];
          continue;
        }
        
        // Y
        var movePointY = (int oldPointY, bool reverse) =>
          (oldPointY + stepFacing * (reverse ? -1 : 1))
          .Mod(notes.Board.Count);
        var newPointY = movePointY(pointY, false);
        var newRow = notes.Board[newPointY];

        if (pointX < newRow.Offset || pointX > newRow.TotalLength)
        {
          do
          {
            newPointY = movePointY(newPointY, true);
            newRow = notes.Board[newPointY];
          } while (pointX >= newRow.Offset && pointX <= newRow.TotalLength);

          newPointY = movePointY(newPointY, false);
          newRow = notes.Board[newPointY];
        }

        if (newRow.WallIndexes.Contains(pointX))
          break;
        
        pointY = newPointY;
        visited[(pointX, pointY)] = Facing[face];
      }

      isTurning = true;
    }

    // Console.WriteLine($"End @({pointX + 1}, {pointY + 1}) facing {(int)face}");
    // notes.Print(visited);
    // System.Console.WriteLine(string.Join(", ", visited.Keys));

    var rowScore = 1000 * (pointY + 1);
    var columnScore = 4 * (pointX + 1);
    var facing = (int)face;

    return rowScore + columnScore + facing;
  }

  static int Part2(string inputPath) => -1;

  static string ToResultFormat(this TimeSpan ts, int res) => $"{res}\t{ts.ToString("s\\.ffffff")}s";

  static int Run(this Stopwatch timer, Func<string, int> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
