﻿using System.Diagnostics;

static class Program
{
  // build blob of cubes from points
  // shift a ghost copy of blob in each of 6 cardinal directions
  // for each ghost copy
  //  count each cube that doesn't intersect with the original blob
  // return sum of counts for approximate surface area
  static HashSet<(int X, int Y, int Z)> ParseCubes(string inputPath)
  {
    var result = new HashSet<(int X, int Y, int Z)>();
    var lines = System.IO.File.ReadLines(inputPath);
    foreach (var line in lines)
    {
      var components = line.Split(',').Select(axis => int.Parse(axis)).ToArray();
      var cube = (X: components[0], Y: components[1], Z: components[2]);
      result.Add(cube);
    }

    return result;
  }

  static int Solve(string inputPath)
  {
    var cubes = ParseCubes(inputPath);

    var cubesXMinus = cubes.Select(xyz => (X: xyz.X - 1, Y: xyz.Y, Z: xyz.Z));
    var cubesXPlus = cubes.Select(xyz => (X: xyz.X + 1, Y: xyz.Y, Z: xyz.Z));
    var cubesYMinus = cubes.Select(xyz => (X: xyz.X, Y: xyz.Y - 1, Z: xyz.Z));
    var cubesYPlus = cubes.Select(xyz => (X: xyz.X, Y: xyz.Y + 1, Z: xyz.Z));
    var cubesZMinus = cubes.Select(xyz => (X: xyz.X, Y: xyz.Y, Z: xyz.Z - 1));
    var cubesZPlus = cubes.Select(xyz => (X: xyz.X, Y: xyz.Y, Z: xyz.Z + 1));

    var countXMinusSurfaceArea = cubesXMinus.Except(cubes).Count();
    var countXPlusSurfaceArea = cubesXPlus.Except(cubes).Count();
    var countYMinusSurfaceArea = cubesYMinus.Except(cubes).Count();
    var countYPlusSurfaceArea = cubesYPlus.Except(cubes).Count();
    var countZMinusSurfaceArea = cubesZMinus.Except(cubes).Count();
    var countZPlusSurfaceArea = cubesZPlus.Except(cubes).Count();

    var totalSurfaceArea = countXMinusSurfaceArea
              + countXPlusSurfaceArea
              + countYMinusSurfaceArea
              + countYPlusSurfaceArea
              + countZMinusSurfaceArea
              + countZPlusSurfaceArea;

    return totalSurfaceArea;
  }

  static int Part1(string inputPath) => Solve(inputPath);
  
  static int Part2(string inputPath) => true ? -1 : Solve(inputPath);

  static string ToResultFormat(this TimeSpan ts, int res) => $"{res}\t{ts.ToString("s\\.ffffff")}s";
  static int Run(this Stopwatch timer, Func<string, int> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }
  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
