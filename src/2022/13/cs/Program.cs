﻿using System.Diagnostics;
using System.Text.Json;

static class Program
{
  static List<JsonElement> ReadPackets(string inputPath)
  {
    var packets = new List<JsonElement>();
    var lines = System.IO.File.ReadLines(inputPath);
    foreach (var line in lines)
    {
      if (string.IsNullOrWhiteSpace(line))
        continue;
      
      packets.Add(JsonSerializer.Deserialize<JsonElement>(line));
    }
    return packets;
  }
  /// <summary>
  /// Compares two JSON values assumed to be either Arrays or Numbers.
  /// </summary>
  /// <param name="left">The first JSON value to compare with.</param>
  /// <param name="right">The second JSON value to compare with.</param>
  /// <returns>
  /// A signed integer indicating the relative value of <paramref name="left">left</paramref> and <paramref name="right">right</paramref>.<br/>
  /// Return Value – Description<br/>
  ///   Less than zero – <paramref name="left">left</paramref> is less than <paramref name="right">right</paramref>.<br/>
  ///   Zero – Both values are equal.<br/>
  ///   Greater than zero – <paramref name="left">left</paramref> is greater than <paramref name="right">right</paramref>.<br/>
  /// </returns>
  static int ComparePackets(JsonElement left, JsonElement right)
  {
    if (left.ValueKind == JsonValueKind.Array)
    {
      if (right.ValueKind == JsonValueKind.Array)
      {
        var leftLength = left.GetArrayLength();
        var rightLength = right.GetArrayLength();
        var commonLength = System.Math.Min(leftLength, rightLength);
        for (int i = 0; i < commonLength; i++)
        {
          var compareArrays = (ComparePackets(left[i], right[i]));
          if (compareArrays != 0)
            return compareArrays;
        }
        var compareArrayLengths = leftLength.CompareTo(rightLength);
        if (compareArrayLengths != 0)
          return compareArrayLengths;
        return 0;
      }
      var compareArrayToNumber = ComparePackets(left, JsonSerializer.SerializeToElement(new object[] { right }));
      return compareArrayToNumber;
    }

    if (right.ValueKind == JsonValueKind.Array)
    {
      var compareNumberToArray = ComparePackets(JsonSerializer.SerializeToElement(new object[] { left }), right);
      return compareNumberToArray;
    }

    var leftNum = left.GetInt32();
    var rightNum = right.GetInt32();
    var compareNumberToNumber = leftNum.CompareTo(rightNum);
    return compareNumberToNumber;
  }
  static int Part1(string inputPath)
  {
    var packets = ReadPackets(inputPath);
    var correctOrderCount = 0;
    for (int i = 0; i < packets.Count - 1; i += 2)
    {
      var pairComparison = ComparePackets(packets[i], packets[i+1]);
      if (pairComparison == -1)
        correctOrderCount += (i >> 1) + 1;
    }
    return correctOrderCount;
  }
  static int FindIndexOf(this List<JsonElement> packets, JsonElement packet)
  {
    var iMin = 0;
    var iMax = packets.Count - 1;
    for (int i = iMax >> 1; i <= iMax;)
    {
      if (iMax - iMin < 0)
        return -1;
      switch (ComparePackets(packet, packets[i]))
      {
        case -1:
          iMax = i - 1;
          i = (iMin + iMax) >> 1;
        break;
        case 1:
          iMin = i + 1;
          i = (iMin + iMax) >> 1; 
        break;
        default:
          return i;
      }
    }
    return -1;
  }
  static int Part2(string inputPath)
  {
    var packets = ReadPackets(inputPath);
    var dividerPackets = new object[]
    { 
      new object[] { new object[] { 2 }},
      new object[] { new object[] { 6 }}
    }.Select(dividerPacket => JsonSerializer.SerializeToElement(dividerPacket));
    foreach (var dividerPacket in dividerPackets)
      packets.Add(dividerPacket);
    packets.Sort(ComparePackets);
    var decoderKey = 1;
    foreach (var dividerPacket in dividerPackets)
    {
      var divIndex = packets.FindIndexOf(dividerPacket);
      if (divIndex != -1)
        decoderKey *= divIndex + 1;
    }
    return decoderKey;
  }
  static string ToResultFormat(this TimeSpan ts, int result) => $"{result}\t{ts.ToString("s\\.ffffff")}s";
  static int Run(this Stopwatch timer, Func<string, int> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }
  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}