﻿using System.Diagnostics;

public static class LongExtensions
{
  private static int BitsInByte = 8;

  public static long Sign(this long a) => a >> sizeof(long) * BitsInByte - 1;
  
  public static long Abs(this long a) => (a + a.Sign()) ^ a.Sign();
  
  public static long Mod(this long a, long b)
  {
    var remainder = a % b;
    return remainder + remainder.Sign().Abs() * b;
  }
}

static class Program
{
  const long DecryptKey = 811589153L;

  record Number(int OriginalIndex, long Value);

  static List<Number> Parse(string inputPath, long scalar = 1L)
  {
    var lines = System.IO.File.ReadLines(inputPath);
    var index = 0;
    var result = new List<Number>();
    foreach (var line in lines)
      result.Add(new Number(index++, long.Parse(line) * scalar));
    
    return result;
  }

  static List<Number> Mix(List<Number> numbers, int rounds = 1)
  {
    var len = numbers.Count;
    for (var round = 0; round < rounds; round++)
      for (var i = 0; i < len; i++)
      {
        var oldIndex = numbers.FindIndex(num => num.OriginalIndex == i);
        var number = numbers[oldIndex];
        var newIndex = (int)(oldIndex + number.Value).Mod(len - 1);

        numbers.RemoveAt(oldIndex);
        numbers.Insert(newIndex, number);
      }

    return numbers;
  }

  static long GetGroveCoordinateSum(List<Number> numbers)
  {
    long zeroIndex = numbers.FindIndex(n => n.Value == 0);
    return Enumerable.Range(1, 3)
      .Sum(offset => numbers[(int)(zeroIndex + offset * 1000L).Mod(numbers.Count)].Value);
  }

  static long Part1(string inputPath) => GetGroveCoordinateSum(Mix(Parse(inputPath)));

  static long Part2(string inputPath) => GetGroveCoordinateSum(Mix(Parse(inputPath, DecryptKey), 10));

  static string ToResultFormat(this TimeSpan ts, long res) => $"{res}\t{ts.ToString("s\\.ffffff")}s";

  static long Run(this Stopwatch timer, Func<string, long> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
