﻿using System.Diagnostics;

static class Program
{ 
  public enum Direction
  {
    North = 0,
    South = 1,
    West = 2,
    East = 3
  }

  public struct Point : IEquatable<Point>
  {
    public int X;
    public int Y;

    public Point(int x, int y)
    {
      X = x;
      Y = y;
    }

    public readonly bool Equals(Point other) =>
      GetType() == other.GetType()
      && X == other.X
      && Y == other.Y;

    public override readonly bool Equals(object? obj) =>
      obj is Point && Equals(obj);
    public override readonly int GetHashCode() => HashCode.Combine(X, Y);
    public static bool operator ==(Point a, Point b) => a.Equals(b);
    public static bool operator !=(Point a, Point b) => !a.Equals(b);
    public static Point operator +(Point a, Point b) => new()
    {
        X = a.X + b.X,
        Y = a.Y + b.Y
    };
    
    public override readonly string ToString() => $"({X},{Y})";
  }

  public static Point ToPoint(this Direction direction)
  {
    return direction switch
    {
      Direction.North => new Point(0, -1),
      Direction.South => new Point(0, 1),
      Direction.West => new Point(-1, 0),
      Direction.East => new Point(1, 0),
      _ => new Point(0, 0)
    };
  }

  public class Elf : IEquatable<Elf>
  {
    public int Id { get; init; }
    public Point Point;

    public Elf(int id, Point position)
    {
      Id = id;
      Point = position;
    }

    public bool Equals(Elf? other) =>
          other != null
      &&  Id == other.Id
      &&  Point == other.Point;
    public override bool Equals(object? obj) => obj is Elf && Equals(obj);
    public override int GetHashCode() => HashCode.Combine(Id, Point);
    public override string ToString() => $"{Id}:{Point}";
  }

  public class Grove
  {
    public Direction Direction = Direction.North;
    public List<Elf> Elves;
    public Dictionary<Point, int> ElfIdByPoint;
    public Point Max;
    public Point Min;

    public Grove(string inputPath)
    {
      Elves = new();

      var y = 0;
      var elfCount = 0;
      var lines = File.ReadLines(inputPath);
      foreach (var line in lines)
      {
        for (var x = 0; x < line.Length; x++)
        {
          if (line[x] != '#')
            continue;
          
          var pos = new Point{ X = x, Y = y };
          var elf = new Elf(elfCount++, pos);
          Elves.Add(elf);
        }

        y++;
      }

      UpdateBounds();

      ElfIdByPoint = Elves.ToDictionary(e => e.Point, e => e.Id);
    }

    public int Width => Math.Abs(Max.X - Min.X) + 1;
    public int Height => Math.Abs(Max.Y - Min.Y) + 1;
    public int Score()
    {
      UpdateBounds();
      return Width * Height - Elves.Count;
    }

    public int SpreadSeeds(int maxRounds = int.MaxValue)
    {
      int rounds = 0;
      bool newPointsApplied;
      do
      {
        // grove.Print();
        // Console.WriteLine();

        var proposedPoints = CreateProposition();
        
        newPointsApplied = ApplyProposition(proposedPoints);

        Direction = Direction.ToNext();
        rounds++;
      }
      while (newPointsApplied && rounds < maxRounds);

      return rounds;
    }
    
    public Dictionary<Point, int> CreateProposition()
    {
      var result = new Dictionary<Point, int>();

      static Point[] ToAdjacent(Point point)
      {
        // -1,-1:NW  0,-1:N  1,-1:NE
        // -1, 0: W          1, 0: E
        // -1, 1:SW  0, 1:S  1, 1:SE
        var adj = new Point[8];
        var adjCount = 0;
        for (int row = -1; row < 2; row++)
        {
          for (int column = -1; column < 2; column++)
          {
            if ((row | column) == 0)
              continue;
            
            adj[adjCount] = point + new Point { X = column, Y = row };
            adjCount++;
          }
        }

        return adj;
      };

      bool ContainsNoneOf(Dictionary<Point, int> pointDict, Point[] points) =>
        !points.Any(p => pointDict.ContainsKey(p));

      var collisions = new HashSet<Point>();
      foreach (var elf in Elves)
      {
        var direction = Direction;
        var adjacent = ToAdjacent(elf.Point);

        if (ContainsNoneOf(ElfIdByPoint, adjacent))
          continue;

        do
        {
          var proposedPoint = elf.Point + direction.ToPoint();

          if (result.ContainsKey(proposedPoint))
          {
            collisions.Add(proposedPoint);
            break;
          }

          var proposedAdjacent = new Point[3];
          switch (direction)
          {
            case Direction.North: 
              Array.Copy(adjacent, 0, proposedAdjacent, 0, 3);
              break;
            case Direction.South:
              Array.Copy(adjacent, 5, proposedAdjacent, 0, 3);
              break;
            case Direction.West:
              proposedAdjacent[0] = adjacent[0];
              proposedAdjacent[1] = adjacent[3];
              proposedAdjacent[2] = adjacent[5];
              break;
            case Direction.East:
              proposedAdjacent[0] = adjacent[2];
              proposedAdjacent[1] = adjacent[4];
              proposedAdjacent[2] = adjacent[7];
              break;
            default:
              break; 
          }

          if (ContainsNoneOf(ElfIdByPoint, proposedAdjacent))
          {
            result[proposedPoint] = elf.Id;
            break;
          }

          direction = direction.ToNext();
        }
        while (!direction.Equals(Direction));
      }

      foreach (var point in collisions)
        result.Remove(point);

      return result;
    }
    
    public bool ApplyProposition(Dictionary<Point, int> pointsToElfIds)
    {
      if (!pointsToElfIds.Any())
        return false;

      foreach (var proposedPoint in pointsToElfIds.Keys)
      {
        var id = pointsToElfIds[proposedPoint];
        var elf = Elves[id];

        ElfIdByPoint.Remove(elf.Point);
        ElfIdByPoint[proposedPoint] = id;
        elf.Point = proposedPoint;
      }

      return true;
    }
    
    public void Print()
    {
      UpdateBounds();
      var xOffset = Min.X;
      var yOffset = Min.Y;
      var width = Width;
      var height = Height;

      var lines = new List<char[]>();

      for (var y = 0; y < height; y++)
      {
        var line = new char[width];
        for (var x = 0; x < width; x++)
        {
          var point = new Point { X = x + xOffset, Y = y + yOffset };
          line[x] = ElfIdByPoint.ContainsKey(point) ? '#' : '.';
        }

        lines.Add(line);
      }

      foreach (var line in lines)
      {
        Console.WriteLine(line);
      }
    }

    public override string ToString() =>
      $"{nameof(Min)}:{Min}, {nameof(Max)}:{Max}, W:{Width}, H:{Height}, " +
      $"S:{Score}, D:{Direction}, E:{Elves.Count}\r\n" +
      $"{string.Join("; ", Elves)}";
    
    private void UpdateBounds()
    {
      var points = Elves.Select(e => e.Point);
      Max.X = points.Max(p => p.X);
      Max.Y = points.Max(p => p.Y);
      Min.X = points.Min(p => p.X);
      Min.Y = points.Min(p => p.Y);
    }
  }

  public static Direction ToNext(this Direction direction)
  {
    var directions = Enum.GetValues<Direction>();
    var result = directions[((int)direction + 1) % directions.Length];
    return result;
  }

  static int Part1(string inputPath)
  {
    var grove = new Grove(inputPath);
    
    _ = grove.SpreadSeeds(10);

    // Console.WriteLine(grove);
    // grove.Print();

    return grove.Score();
  }

  static int Part2(string inputPath)
  {
    var grove = new Grove(inputPath);

    var roundsCompleted = grove.SpreadSeeds(1000);

    return roundsCompleted;
  }

  static string ToResultFormat(this TimeSpan ts, int res) => $"{res}\t{ts.ToString("s\\.ffffff")}s";

  static int Run(this Stopwatch timer, Func<string, int> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
