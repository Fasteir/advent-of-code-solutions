﻿using System.Diagnostics;

public static class LongExtentions
{
  public static long  Sign(this long  a) => a >> sizeof(long ) * 8 - 1;
  public static long  Abs(this long  a) => (a + a.Sign()) ^ a.Sign();
  public static long  Dist(this long  a, long  b) => (a - b).Abs();
}

static class Program
{ 
  static long  ManhattanDistance((long  X, long  Y) a, (long  X, long  Y) b) =>
    a.X.Dist(b.X) + a.Y.Dist(b.Y);

  static HashSet<((long  X, long  Y) S, (long  X, long  Y) B, long  D)> ParseLines(string input)
  {
    var lines = System.IO.File.ReadLines(input);
    var map = new HashSet<((long  X, long  Y) S, (long  X, long  Y) B, long  D)>();
    foreach (var line in lines)
    {
      var sensorXIndex = line.IndexOf("x=") + 2;
      var sensorXLength = line.IndexOf(',') - sensorXIndex;
      var sensorX = long .Parse(line.Substring(sensorXIndex, sensorXLength));
      var sensorYIndex = line.IndexOf("y=") + 2;
      var sensorYLength = line.IndexOf(':') - sensorYIndex;
      var sensorY = long .Parse(line.Substring(sensorYIndex, sensorYLength));
      var sensor = (X: sensorX, Y: sensorY);

      var beaconXIndex = line.LastIndexOf("x=") + 2;
      var beaconXLength = line.LastIndexOf(',') - beaconXIndex;
      var beaconX = long .Parse(line.Substring(beaconXIndex, beaconXLength));
      var beaconY = long .Parse(line.Substring(line.LastIndexOf("y=") + 2));
      var beacon = (X: beaconX, Y: beaconY);

      var distance = ManhattanDistance(sensor, beacon);
      map.Add((sensor, beacon, distance));
    }

    return map;
  }

  static long  Part1(string inputPath)
  {
    var map = ParseLines(inputPath);    
    var yRow = 2000000;
    var sensors = map.Select(sbd => sbd.S);
    var beacons = map.Select(sbd => sbd.B).Distinct();
    var distanceMax = map.Max(sbd => sbd.D);

    var negativeXMap = new HashSet<long>();
    foreach (var sbd in map)
    {
      if (sbd.S.Y < yRow - distanceMax || sbd.S.Y > yRow + distanceMax)
        continue;

      var xMin = sbd.S.X - sbd.D + (sbd.S.Y - yRow).Abs(); 
      var xMax = sbd.S.X + sbd.D - (sbd.S.Y - yRow).Abs();
      for (var x = xMin; x <= xMax; x++)
        negativeXMap.Add(x);
    }
    var negativeSpacesCount = negativeXMap.Count
                            - sensors.Count(s => s.Y == yRow)
                            - beacons.Count(b => b.Y == yRow);

    return negativeSpacesCount;
  }

  static long  Part2(string inputPath)
  {
    return -1;
  }

  static string ToResultFormat(this TimeSpan ts, long  res) => $"{res}\t{ts.ToString("s\\.ffffff")}s";

  static long  Run(this Stopwatch timer, Func<string, long > solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}