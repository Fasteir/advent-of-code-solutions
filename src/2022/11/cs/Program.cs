﻿static class Program
{
  public class Monkey
  {
    public int Id;
    public List<ulong> Items = Enumerable.Empty<ulong>().ToList();
    public Func<ulong, ulong> Operation = (ulong a) => a;
    public ulong TestDivisor;
    public ulong TestPositiveMonkeyId;
    public ulong TestNegativeMonkeyId;
    public ulong Inspections;
    public static List<Monkey> ParseMonkeys(IEnumerable<string> lines)
    {
      var monkeys = Enumerable.Empty<Monkey>().ToList();
      var isNewMonkey = false;
      var monkeyId = 0;
      List<ulong> startingItems = Enumerable.Empty<ulong>().ToList();
      Func<ulong, ulong> operation = (ulong a) => a;
      ulong testDivisor = 1ul;
      ulong testPositiveMonkeyId = 0ul;
      ulong testNegativeMonkeyId = 0ul;
      
      foreach (var line in lines)
      {
        if (string.IsNullOrWhiteSpace(line))
          continue;
          
        if(line.StartsWith("Monkey"))
          isNewMonkey = true;
        
        if (isNewMonkey && line.StartsWith("  S"))
        {
            startingItems = line.Substring(line.LastIndexOf(':') + 2)
            .Split(", ")
            .Select(item => ulong.Parse(item))
            .ToList();
        }
        
        var lineValue = line.Substring(line.LastIndexOf(' ') + 1);
        
        if (isNewMonkey && line.StartsWith("  O"))
        {
          var useOld = lineValue.Equals("old");
          var modifier = useOld ? 0 : ulong.Parse(lineValue);
          var op = line[line.LastIndexOf("=") + 6];
          operation = op switch
          {
            '*' => useOld ? (ulong worryLevel) => worryLevel * worryLevel
                          : (ulong worryLevel) => worryLevel * modifier,
            _ =>  useOld  ? (ulong worryLevel) => worryLevel + worryLevel
                          : (ulong worryLevel) => worryLevel + modifier
          };
        }

        if (isNewMonkey && line.StartsWith("  T"))
          testDivisor = ulong.Parse(lineValue);

        if (isNewMonkey && line.StartsWith("    If t"))
          testPositiveMonkeyId = ulong.Parse(lineValue);

        if (isNewMonkey && line.StartsWith("    If f"))
        {
          testNegativeMonkeyId = ulong.Parse(lineValue);
          monkeys.Add(new Monkey
          {
            Id = monkeyId++,
            Items = startingItems,
            Operation = operation,
            TestDivisor = testDivisor,
            TestPositiveMonkeyId = testPositiveMonkeyId,
            TestNegativeMonkeyId = testNegativeMonkeyId
          });
          isNewMonkey = false;
        }
      }

      return monkeys;
    }
  }
  static ulong Product(this IEnumerable<ulong> collection) => collection.Aggregate(1ul, (acc, x) => acc * x);
  static ulong Solve(string inputPath, int rounds, bool runawayWorrying = false)
  {
    var lines = System.IO.File.ReadLines(inputPath);
    
    var monkeys = Monkey.ParseMonkeys(lines);
    var commonDivisor = monkeys.Select(m => m.TestDivisor).Product();
    var monkeyBusiness = 1ul;

    var inspectItems = (Monkey monkey) =>
    {
        while(monkey.Items.Any())
        {
          var item = monkey.Operation(monkey.Items[0]);
          item = runawayWorrying ? item % commonDivisor : item / 3;
          _ = monkeys[monkey.Id].Inspections++;
          var recipientMonkeyId = item % monkey.TestDivisor == 0L
            ? monkey.TestPositiveMonkeyId
            : monkey.TestNegativeMonkeyId;
          
          monkeys[monkey.Id].Items.RemoveAt(0);
          monkeys[(int)recipientMonkeyId].Items.Add(item);
        }
    };

    for (int round = 0; round < rounds; round++)
      foreach(var monkey in monkeys)
        inspectItems(monkey);

    monkeyBusiness = monkeys.Select(monkey => monkey.Inspections)
      .OrderByDescending(inspection => inspection)
      .Take(2)
      .Product();

    return monkeyBusiness;
  }
  static string ToResultFormat(this TimeSpan ts, ulong result) => $"{result}\t{ts.ToString("s\\.ffffff")}s";
  static ulong Run(this System.Diagnostics.Stopwatch timer, Func<string, int, bool, ulong> solution, string inputPath, int rounds = 20, bool runawayWorrying = false)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath, rounds, runawayWorrying);
    timer.Stop();
    return result;
  }
  static void Main(string[] args)
  {
    var timer = new System.Diagnostics.Stopwatch();
    var warmup = timer.Run(Solve, args[0]);

    var part1 = timer.Run(Solve, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    
    var part2 = timer.Run(Solve, args[0], 10000, true);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}