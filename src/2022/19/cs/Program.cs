﻿using System.Diagnostics;

public enum Resource
{
  None = -1,
  Ore = 0,
  Clay = 1,
  Obsidian = 2,
  Geode = 3
}

public sealed class ResourceState
{
  public int TimeLeft { get; init; }
  public int[] Resources { get; init; } = new int[Enum.GetValues<Resource>().Count(r => r >= 0)];
  public int[] Robots { get; init; } = new int[Enum.GetValues<Resource>().Count(r => r >= 0)];
  public int OmitBuilding { get; init; } = 0;
  public int MaxGeodes => Resources[(int)Resource.Geode]
    + (((Robots[(int)Resource.Geode] << 1) + TimeLeft - 1) * TimeLeft >> 1);
}

public record Blueprint(int Id, int[][] RobotCosts)
{
  public int[] MaxCosts = Enum.GetValues<Resource>()
    .Where(resource => resource != Resource.None)
    .Select(resource => resource == Resource.Geode
      ? int.MaxValue
      : RobotCosts.Max(costs => costs[(int)resource]))
    .ToArray();
}

static class Program
{
  static IEnumerable<Blueprint> ParseBlueprints(string inputPath)
  {
    var lines = System.IO.File.ReadLines(inputPath);
    foreach (var line in lines)
    {
      var bpArgs = System.Text.RegularExpressions.Regex.Matches(line, "(\\d+)")
        .Select(match => int.Parse(match.Value))
        .ToArray();
      var id = bpArgs[0];
      var costs = new int[][]
      {
        //      Ore,       Clay,      Obsidian,   Geode
        new[] { bpArgs[1], 0,         0,          0 },  // Ore      Robot
        new[] { bpArgs[2], 0,         0,          0 },  // Clay     Robot
        new[] { bpArgs[3], bpArgs[4], 0,          0 },  // Obsidian Robot
        new[] { bpArgs[5], 0,         bpArgs[6],  0 }   // Geode    Robot
      };
      yield return new Blueprint(id, costs);
    }
  }

  static bool MeetsBuildCriteria(Blueprint bp, ResourceState state, Resource newRobot) =>
        0 == (state.OmitBuilding & 1 << (int)newRobot)
    &&  state.Robots
          .Select((count, resource) => bp.MaxCosts[resource] >= (resource == (int)newRobot
            ? count + 1
            : count))
          .All(countDoesNotExceedMaxCost => countDoesNotExceedMaxCost);

  static int GetMaxGeodes(Blueprint blueprint, int timeLeft)
  {
    var initialState = new ResourceState()
    {
      TimeLeft = timeLeft,
      Robots = new[] { 1, 0, 0, 0 }
    };

    var queue = new PriorityQueue<ResourceState, int>();
    var cache = new HashSet<ResourceState>();

    var enqueue = (ResourceState state) => queue.Enqueue(state, -state.MaxGeodes);
    enqueue(initialState);

    var maxGeodes = 0;
    while (queue.Count > 0)
    {
      var state = queue.Dequeue();
      
      if (state.MaxGeodes < maxGeodes)
        break;

      if (cache.Contains(state))
        continue;

      cache.Add(state);

      if (state.TimeLeft < 1)
      {
        maxGeodes = Math.Max(maxGeodes, state.Resources[(int)Resource.Geode]);
        continue;
      }

      var affordableRobots = Enum.GetValues<Resource>().Where(resource =>
            resource != Resource.None
        &&  blueprint.RobotCosts[(int)resource]
              .Select((cost, resource) => state.Resources[resource] - cost >= 0)
              .All(isAffordable => isAffordable));
      
      var pendingRobots = affordableRobots
        .Where(robot => MeetsBuildCriteria(blueprint, state, robot))
        .Append(Resource.None);
      
      foreach (var robot in pendingRobots)
        enqueue(new ResourceState
        {
          TimeLeft = state.TimeLeft - 1,
          Resources = state.Resources
            .Select((count, resource) =>
                count
              + state.Robots[resource]
              - (robot == Resource.None ? 0 : blueprint.RobotCosts[(int)robot][resource]))
            .ToArray(),
          Robots = state.Robots
            .Select((count, resource) => resource == (int)robot ? count + 1 : count)
            .ToArray(),
          OmitBuilding = robot == Resource.None
            ? affordableRobots.Sum(robotType => 1 << (int)robotType)
            : 0
        });
    }

    return maxGeodes;
  }

  static Dictionary<int, int> Solve(string inputPath, int duration, int blueprintMax = int.MaxValue)
  {
    var blueprints = ParseBlueprints(inputPath).Where(bp => bp.Id <= blueprintMax);

    var blueprintMaxGeodes = new Dictionary<int, int>();
    foreach (var bp in blueprints)
      blueprintMaxGeodes[bp.Id] = GetMaxGeodes(bp, duration);
    
    return blueprintMaxGeodes;
  }

  static int Part1(string inputPath)
  {
    var blueprintMaxGeodes = Solve(inputPath, 24);
    var qualityLevelSum = blueprintMaxGeodes.Keys.Sum(bpId => bpId * blueprintMaxGeodes[bpId]);

    return qualityLevelSum;
  }

  static int Part2(string inputPath)
  {
    var blueprintMaxGeodes = Solve(inputPath, 32, 3);
    var maxGeodesProduct = blueprintMaxGeodes.Values.Aggregate(1, (acc, geodes) => acc * geodes);

    return maxGeodesProduct;
  }

  static string ToResultFormat(this TimeSpan ts, int res) => $"{res}\t{ts.ToString("s\\.ffffff")}s";
  
  static int Run(this Stopwatch timer, Func<string, int> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
