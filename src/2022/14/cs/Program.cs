﻿using System.Diagnostics;

public static class IntExtentions
{
  public static int Sign(this int a) => a >> sizeof(int) * 8 - 1;
  public static int Abs(this int a) => (a + a.Sign()) ^ a.Sign();
  public static int Dist(this int a, int b) => (a - b).Abs();
  public static int Gt(this int a, int b) => (b - a).Sign().Abs();
  public static int Eq(this int a, int b) => 1 + ((a-b) | (b-a)).Sign();
  public static int Not(this int a) => a ^ 1;
}
static class Program
{
  const char Solid = '#';
  const char Air = '.';

  static (int X, int Y)[] ParsePath(string line) =>
    line.Split(" -> ")
        .Select(xy => xy.Split(',').Select(axis => int.Parse(axis)))
        .Select(point => (X: point.First(), Y: point.Last()))
        .ToArray();

  static IEnumerable<(int X, int Y)> CardinalInterpolate((int X, int Y) a, (int X, int Y) b)
  {
    // Ensure start < final
    var xDist = a.X.Dist(b.X);
    var xOffset = a.X.Gt(b.X) * xDist;
    var xStart = a.X - xOffset;
    var xFinal = b.X + xOffset;

    var yDist = a.Y.Dist(b.Y);
    var yOffset = a.Y.Gt(b.Y) * yDist;
    var yStart = a.Y - yOffset;
    var yFinal = b.Y + yOffset;

    // Progress x and y according to whether line is vertical or horizontal
    var isVertical = xDist.Eq(0);
    var start = isVertical * yStart + isVertical.Not() * xStart;
    var final = isVertical * yFinal + isVertical.Not() * xFinal;
    var x = (int x, int defaultX) => isVertical * defaultX + isVertical.Not() * x;
    var y = (int y, int defaultY) => isVertical * y        + isVertical.Not() * defaultY;

    for (var i = start; i <= final; i++)
      yield return (x(i, a.X), y(i, a.Y));
  }

  static void PopulateMap(ref HashSet<(int X, int Y)> map, (int X, int Y)[] path)
  {
    for (int pointEnd = 0; pointEnd < path.Length - 1; pointEnd++)
    {
      var pointsLine = CardinalInterpolate(path[pointEnd], path[pointEnd+1]);
      foreach (var point in pointsLine)
        map.Add(point);
    }
  }

  static HashSet<(int X, int Y)> ParseCave(string inputPath)
  {
    var map = new HashSet<(int X, int Y)>();
    var lines = System.IO.File.ReadLines(inputPath);
    foreach (var line in lines)
    {
      var path = ParsePath(line);
      PopulateMap(ref map, path);
    }

    return map;
  }

  static (bool Settled, (int X, int Y) SettlePoint) DropSand(HashSet<(int X, int Y)> map, (int X, int Y) sourcePoint, int depthMax)
  {
    var sandPoint = sourcePoint;
    while(sandPoint.Y < depthMax)
    {
      sandPoint.Y++; 
      if (false == map.Contains(sandPoint))
        continue;
      
      sandPoint.X--;
      if (false == map.Contains(sandPoint))
        continue;
      
      sandPoint.X += 2;
      if (false == map.Contains(sandPoint))
        continue;

      sandPoint.X--;
      sandPoint.Y--;
      return (true, sandPoint);
    }

    return (false, sandPoint);
  }

  static void Print(HashSet<(int X, int Y)> map)
  {
    var xMin = map.Min(xy => xy.X);
    var xMax = map.Max(xy => xy.X);
    var yMin = map.Min(xy => xy.Y);
    var yMax = map.Max(xy => xy.Y);
    for (int y = yMin; y <= yMax; y++)
    {
      for (int x = xMin; x <= xMax; x++)
        Console.Write(map.Contains((x, y)) ? Solid : Air);
      Console.WriteLine();
    }
  }

  static int Part1(string inputPath)
  {
    var sandSource = (X: 500, Y: 0);
    var map = ParseCave(inputPath);
    var depth = map.Max(point => point.Y);
    // Print(map);

    var restingSandAmount = 0;
    while (true)
    { 
      var result = DropSand(map, sandSource, depth);
      if (false == result.Settled)
        break;

      map.Add(result.SettlePoint);
      restingSandAmount++;
    }
    // Print(map);

    return restingSandAmount;
  }

  static int Part2(string inputPath)
  {
    var sandSource = (X: 500, Y: 0);
    var map = ParseCave(inputPath);
    int depth = map.Max(point => point.Y) + 2;
    
    var floorPath = new[]
    {
      (X: sandSource.X - depth, Y: depth),
      (X: sandSource.X + depth, Y: depth)
    };
    PopulateMap(ref map, floorPath);
    // Print(map);

    var restingSandAmount = 0;
    while (true)
    { 
      var result = DropSand(map, sandSource, depth);
      if (false == result.Settled)
        break;

      map.Add(result.SettlePoint);
      restingSandAmount++;

      if (result.SettlePoint.Equals(sandSource))
        break;
    }
    // Print(map);

    return restingSandAmount;
  }

  static string ToResultFormat(this TimeSpan ts, int res) => $"{res}\t{ts.ToString("s\\.ffffff")}s";

  static int Run(this Stopwatch timer, Func<string, int> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}