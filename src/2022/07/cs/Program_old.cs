﻿public class FileTreeNode
{
  public int Size = 0;
  public bool IsDir = false;
  public string Name = "";
  public FileTreeNode? Parent = null;
  public Dictionary<string, FileTreeNode> Children = new Dictionary<string, FileTreeNode>();
  public int UpdateSize()
  {
    if (IsDir)
      Size = 0;
    foreach (var child in Children.Keys)
      Size += Children[child].UpdateSize();
    return Size;
  }
  public void AddChild(FileTreeNode child)
  {
    Children.Add(child.Name, child);
    child.Parent = this;
  }
  public string PrintAll(string indent = "")
  {
    var sb = new System.Text.StringBuilder();
    sb.Append(ToString());
    foreach (var child in Children.Keys)
      sb.Append($"{Environment.NewLine}{indent}{Children[child].PrintAll(indent+"  ")}");
    return sb.ToString();
  }
  public override string ToString()
  {
    var info = (IsDir ? "dir" : "file") + (Size == 0 ? "" : $", size={Size}");
    return $"- {Name} ({info})";
  }
}
public class FileTree
{
  public FileTreeNode Root = new FileTreeNode()
  {
    Name = "/",
    IsDir = true
  };
  public int Size => Root.Size;
  public override string ToString() => Root.PrintAll("  ");
}
public class ProgramOld
{
  public static FileTree ParseFileTree(IEnumerable<string> lines)
  {
    var tree = new FileTree();
    FileTreeNode current = tree.Root;
    var handleCommandCd = (string dir) =>
    {
      current = dir switch {
        ".." => current?.Parent ?? tree.Root,
        "/" => tree.Root,
        _ => current.Children[dir]
      };
    };
    var handleCommand = (string[] commandAndArgs) =>
    {
      if (commandAndArgs[1].Equals("cd"))
          handleCommandCd(commandAndArgs[2]);
    };
    foreach (var line in lines)
    {
      var splitLine = line.Split(' ');
      switch (splitLine[0])
      {
        case "$":
          handleCommand(splitLine);
          break;
        case "dir":
          current.AddChild(new FileTreeNode()
          {
            Name = splitLine[1],
            IsDir = true
          });
          break;
        default:
          _ = int.TryParse(splitLine[0], out int size);
          current.AddChild(new FileTreeNode()
          {
            Name = splitLine[1],
            Size = size
          });
          break;
      }
    }
    tree.Root.UpdateSize();
    return tree;
  }
  public static List<FileTreeNode> TreeDirsByMaxSize(FileTree tree, int sizeMax)
  {
    if (sizeMax < 0 || tree.Root == null)
      return Enumerable.Empty<FileTreeNode>().ToList();
    return TreeDirsByMaxSizeR(tree.Root, sizeMax);
  }
  private static List<FileTreeNode> TreeDirsByMaxSizeR(FileTreeNode node, int sizeMax)
  {
    var dirs = new List<FileTreeNode>();
    if (node.IsDir && (node.Size <= sizeMax || sizeMax == 0))
      dirs.Add(node);
    if (!node.Children.Any())
      return dirs;
    foreach(var child in node.Children.Keys)
      dirs.AddRange(TreeDirsByMaxSizeR(node.Children[child], sizeMax));
    return dirs;
  }
  public static void MainOld(string[] args)
  {
    var formatTimeElapsed = (int result, TimeSpan ts) => $"{result}\t{ts.ToString("s\\.ffffff")}s";
    var timer = new System.Diagnostics.Stopwatch();
    FileTree tree;
    List<FileTreeNode> dirs;
    var sizeDirs = 0;
    var lines = System.IO.File.ReadLines(args[0]);
    
    // warmup
    timer.Start();
    tree = ParseFileTree(lines);
    dirs = TreeDirsByMaxSize(tree, 100000);
    sizeDirs = dirs.Sum(n => n.Size);
    timer.Stop();
    timer.Reset();

    // part 1
    timer.Start();
    tree = ParseFileTree(lines);
    dirs = TreeDirsByMaxSize(tree, 100000);
    sizeDirs = dirs.Sum(dir => dir.Size);
    timer.Stop();
    Console.WriteLine(tree);
    Console.WriteLine(formatTimeElapsed(sizeDirs, timer.Elapsed));
    timer.Reset();
    
    // part 2
    timer.Start();
    tree = ParseFileTree(lines);
    dirs = TreeDirsByMaxSize(tree, 0);
    var totalDiskSpace = 70000000;
    var sizeNeeded = 30000000;
    var sizeToFree = sizeNeeded - totalDiskSpace + tree.Size;
    var minimalDirToDeleteSize = 0;
    if (sizeToFree > 0)
      minimalDirToDeleteSize = dirs.Where(d => d.Size >= sizeToFree).Min(d => d.Size);
    timer.Stop();
    Console.WriteLine(formatTimeElapsed(minimalDirToDeleteSize, timer.Elapsed));
  }
}
