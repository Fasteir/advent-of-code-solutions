﻿public class Program
{
  public static string GetParentPath(string path)
  {
    var parentIndex = path.LastIndexOf('/');
    if (path.Equals("/") || parentIndex != 0)
      return path.Substring(0, parentIndex);
    return "/";
  }
  public static Dictionary<string, int> ParsePathTree(IEnumerable<string> lines)
  {
    var result = new Dictionary<string, int>();
    var updatePathDirSizes = (string path, int size) =>
    {
      do
        result[path] = size + (result.ContainsKey(path) ? result[path] : 0);
      while ((path = GetParentPath(path)) != "");
    };

    var currentPath = "";
    foreach (var line in lines)
    {
      var lineParts = line.Split(' ');
      if (lineParts[0].Equals("dir"))
        continue;
      if (lineParts[0].Equals("$") == false)
      {
        _ = int.TryParse(lineParts[0], out int size);
        updatePathDirSizes(currentPath, size);
        continue;
      }
      if (lineParts[1].Equals("cd"))
        currentPath = lineParts[2] switch
        {
          "/" => "/",
          ".." => GetParentPath(currentPath),
          _ => currentPath + (currentPath.Equals("/") ? "" : "/") + lineParts[2]
        };
    }
    return result;
  }
  public static void Main(string[] args)
  {
    var formatTimeElapsed = (int result, TimeSpan ts) => $"{result}\t{ts.ToString("s\\.ffffff")}s";
    var timer = new System.Diagnostics.Stopwatch();
    var lines = System.IO.File.ReadLines(args[0]);
    
    // warmup
    timer.Start();
    var dirs = ParsePathTree(lines);
    var filteredDirsSizeSum = dirs.Keys.Sum(dir => dirs[dir]);
    timer.Stop();
    timer.Reset();

    // part 1
    timer.Start();
    dirs = ParsePathTree(lines);
    filteredDirsSizeSum = 0;
    foreach (var dirKey in dirs.Keys)
      if (dirs[dirKey] <= 100000)
        filteredDirsSizeSum += dirs[dirKey];
    timer.Stop();
    Console.WriteLine(formatTimeElapsed(filteredDirsSizeSum, timer.Elapsed));
    timer.Reset();
    
    // part 2
    timer.Start();
    dirs = ParsePathTree(lines);
    var totalDiskSpace = 70000000;
    var sizeNeeded = 30000000;
    var sizeToFree = sizeNeeded - totalDiskSpace + dirs["/"];
    var minimalDirToDeleteSize = int.MaxValue;
    if (sizeToFree > 0)
      foreach (var dirKey in dirs.Keys)
        if (dirs[dirKey] >= sizeToFree && dirs[dirKey] < minimalDirToDeleteSize) 
          minimalDirToDeleteSize = dirs[dirKey];
    timer.Stop();
    Console.WriteLine(formatTimeElapsed(minimalDirToDeleteSize, timer.Elapsed));
    timer.Reset();
  }
}
