﻿using System.Diagnostics;
using System.Text.RegularExpressions;

static class Program
{
  static Dictionary<string, int> ColorMaxes = new()
  {
    { "red", 12 },
    { "green", 13 },
    { "blue", 14}
  };

  static bool GameIsPossible(string line)
  {
    foreach (var color in ColorMaxes.Keys)
    {
      var isPossibleForColor = Regex.Matches(line, $"(\\d+) {color}")
        .Select(match => match.Groups[1].Value)
        .All(count => int.Parse(count) <= ColorMaxes[color]);
      
      if (!isPossibleForColor)
        return false;
    }
    
    return true;
  }
    static int Part1(string inputPath)
  {
    var idSum = 0;
    var lines = File.ReadLines(inputPath);
    foreach (var line in lines)
    {
      var cursor = line.IndexOf(' ') + 1;
      var digitEndIndex = line.IndexOf(':', cursor);
      var id = int.Parse(line[cursor..digitEndIndex]);
      
      if (GameIsPossible(line))
        idSum += id;
    }

    return idSum;
  }

  static int Part2(string inputPath) => -1;

  static string ToResultFormat(this TimeSpan ts, int res) => $"{res}\t{ts.TotalMilliseconds}ms";

  static int Run(this Stopwatch timer, Func<string, int> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
