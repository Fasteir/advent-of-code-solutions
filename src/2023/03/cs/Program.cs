﻿using System.Diagnostics;

static class Program
{
  record struct Number(string Value, int StartX, int StartY);

  record struct Symbol(int X, int Y);

  record Schematic(IEnumerable<Number> Numbers, IEnumerable<Symbol> Symbols)
  {
    public bool NumberIsPart(Number number)
    {
      var adjacentX = Enumerable.Range(number.StartX - 1, number.Value.Length + 2);
      var adjacentY = Enumerable.Range(number.StartY - 1, 3);

      var isPartNumber = Symbols.Any(s => adjacentX.Contains(s.X) && adjacentY.Contains(s.Y));

      return isPartNumber;
    }
  }

  static Schematic Parse(string inputPath)
  {
    var lines = File.ReadLines(inputPath);
    var lineY = -1;
    
    var numbers = new List<Number>();
    var symbols = new List<Symbol>();
    foreach (var line in lines)
    {
      lineY++;
      
      for (int lineX = 0; lineX < line.Length; lineX++)
      {
        if (line[lineX] == '.')
          continue;

        if (!char.IsDigit(line[lineX]))
        {
          symbols.Add(new Symbol(lineX, lineY));
          continue;
        }

        var nextNonNumberX = lineX;
        while(nextNonNumberX < line.Length
          && char.IsDigit(line[nextNonNumberX]))
          nextNonNumberX++;

        var numberValue = line[lineX..nextNonNumberX];
        numbers.Add(new Number(numberValue, lineX, lineY));
        lineX = nextNonNumberX - 1;
      }
    }

    return new Schematic(numbers, symbols);
  }

  static int Part1(string inputPath)
  {
    var schematic = Parse(inputPath);

    var result = 0;
    foreach (var number in schematic.Numbers)
      if (schematic.NumberIsPart(number))
        result += int.Parse(number.Value);

    return result;
  }

  static int Part2(string inputPath) => -1;

  static string ToResultFormat(this TimeSpan ts, int res) => $"{res}\t{ts.TotalMilliseconds}ms";

  static int Run(this Stopwatch timer, Func<string, int> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
