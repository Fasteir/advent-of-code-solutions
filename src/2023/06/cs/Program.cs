﻿using System.Diagnostics;

static class Program
{
  record struct Race(int Duration, int RecordDistance)
  {
    public int MaxHoldDuration = Duration >> 1;
    public int MaxDistance = (Duration >> 1) * (Duration - (Duration >> 1));
  }

  static IEnumerable<Race> ParseRaces(string inputPath)
  {
    var lines = File.ReadLines(inputPath)
      .Select(line => line[(line.IndexOf(':') + 1)..]
        .Split(' ', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries)
        .Select(int.Parse)
        .ToArray())
      .ToArray();
    
    var result = lines
      .First()
      .Select((duration, recordDistanceIndex) => new Race(duration, lines[1][recordDistanceIndex]));

    return result;
  }

  static long Part1(string inputPath)
  {
    var races = ParseRaces(inputPath);

    var spreadProduct = 1;
    foreach (var race in races)
    {
      /*
       *  Let t = race duration
       *  Let d = race record distance
       *  Let h = hold duration
       *  Let v = speed
       *
       *  v = h
       *  d = v * (t - h)
       *  0 = -h^2 + th - d
       *  
       *  By quadratic formula "(-b +- sqrt(b^2 - 4ac)) / 2a":
       *  h = (-(t) +- sqrt((t)^2 - 4(-1)(-d))) / 2(-1)
       *  h_min = (t - sqrt(t^2 - 4d)) / 2
       *  h_max = (t + sqrt(t^2 - 4d)) / 2
       *
       *  Count record-beating integer values of h within the bounds found above.
       */
      var deviation = Math.Sqrt(race.Duration * race.Duration - 4 * race.RecordDistance);
      var min = (int)Math.Floor(1 + (race.Duration - deviation) / 2);
      var max = (int)Math.Ceiling(-1 + (race.Duration + deviation) / 2);

      var spread = max - min + 1;
      spreadProduct *= spread;
    }

    return spreadProduct;
  }

  static long Part2(string inputPath) => -1;

  static string ToResultFormat(this TimeSpan ts, long res) => $"{res}\t{ts.TotalMilliseconds}ms";

  static long Run(this Stopwatch timer, Func<string, long> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
