﻿using System.Diagnostics;

static class Program
{
  static long Part1(string inputPath)
  {
    var lines = File.ReadLines(inputPath);

    var parseSeeds = true;
    long[]? seeds = default;
    int mappedSeeds = 0;
    var lineNumber = 0;
    foreach (var line in lines)
    {
      lineNumber++;
      if (parseSeeds)
      {
        parseSeeds = false;
        seeds = line[(line.IndexOf(' ') + 1)..]
          .Split(' ')
          .Select(long.Parse)
          .ToArray();
        continue;
      }

      if (line.Length < 1)
      {
        mappedSeeds = 0;
        continue;
      }

      if (line.LastIndexOf(':') > -1)
        continue;

      var map = line.Split(' ');
      for (int i = 0; i < (seeds?.Length ?? 0); i++)
      {
        var seed = seeds?[i] ?? 0L;
        var dest = long.Parse(map[0]);
        var src = long.Parse(map[1]);
        var len = long.Parse(map[2]);
        if (  seed < src
           || seed > src + len - 1
           || 0 < (mappedSeeds & (1 << i)))
          continue;
        
        seeds[i] = dest - src + seed;
        mappedSeeds |= 1 << i;
      }

      // Console.WriteLine($"{lineNumber}\t{string.Join("\t", seeds)}");
    }

    return seeds?.Min() ?? -1;
  }

  static long Part2(string inputPath) => -1;

  static string ToResultFormat(this TimeSpan ts, long res) => $"{res}\t{ts.TotalMilliseconds}ms";

  static long Run(this Stopwatch timer, Func<string, long> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    // var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
