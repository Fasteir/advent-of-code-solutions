﻿using System.Diagnostics;

static class Program
{
  static int Part1(string inputPath)
  {
    var sum = 0;
    var lines = File.ReadLines(inputPath);
    foreach (var line in lines)
    {
      var lineSum = 0;
      var winningIndex = line.IndexOf(':');
      var actualIndex = line.IndexOf('|', winningIndex + 2);

      static List<string> parseNumbers(string str, int start, int end)
      {
        var result = new List<string>();
        var sb = new System.Text.StringBuilder();
        for (int i = start; i < end; i++)
        {
          var ch = str[i];
          if (ch == ' ')
          {
            if (sb.Length > 0)
            {
              result.Add(sb.ToString());
              sb.Clear();
            }

            continue;
          }
          
          sb.Append(ch);
        }

        if (sb.Length > 0)
          result.Add(sb.ToString());

        sb.Clear();

        return result;
      };
      
      var winners = parseNumbers(line, winningIndex + 2, actualIndex - 1);
      var actuals = parseNumbers(line, actualIndex + 2, line.Length);

      foreach (var winner in winners)
      {
        if (actuals.Contains(winner))
        {
          actuals.Remove(winner);
          lineSum = lineSum == 0 ? 1 : lineSum << 1;
        }
      }

      sum += lineSum;
    }

    return sum;
  }

  static int Part2(string inputPath) => -1;

  static string ToResultFormat(this TimeSpan ts, int res) => $"{res}\t{ts.TotalMilliseconds}ms";

  static int Run(this Stopwatch timer, Func<string, int> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
