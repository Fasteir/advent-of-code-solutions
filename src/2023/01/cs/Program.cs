﻿using System.Diagnostics;

static class Program
{
  static int Part1(string inputPath)
  {
    var result = 0;
    var digits = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
    var lines = File.ReadLines(inputPath);
    foreach (var line in lines)
    {
      result += 10 * (line[line.IndexOfAny(digits)] - '0');
      result += line[line.LastIndexOfAny(digits)] - '0';
    }

    return result;
  }

  static int Part2(string inputPath) => -1;

  static string ToResultFormat(this TimeSpan ts, int res) => $"{res}\t{ts.TotalMilliseconds}ms";

  static int Run(this Stopwatch timer, Func<string, int> solution, string inputPath)
  {
    timer.Reset();
    timer.Start();
    var result = solution(inputPath);
    timer.Stop();
    return result;
  }

  static void Main(string[] args)
  {
    var timer = new Stopwatch();
    var warmup = timer.Run(Part1, args[0]);
    var part1 = timer.Run(Part1, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part1));
    var part2 = timer.Run(Part2, args[0]);
    System.Console.WriteLine(timer.Elapsed.ToResultFormat(part2));
  }
}
